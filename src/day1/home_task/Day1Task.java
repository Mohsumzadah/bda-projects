package day1.home_task;

import java.util.Locale;
import java.util.Scanner;

public class Day1Task {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, write your full name.");
        greetingMessage(scanner.nextLine());
    }

    // Salamla mesajının print edilməsi.
    private static void greetingMessage(String fullName){
        fullName = fullName.toLowerCase(Locale.ROOT);

        String firstName = firstLetterUpperCase(fullName.split(" ")[0]);
        String secondName = firstLetterUpperCase(fullName.split(" ")[1]);

        System.out.println("Hello,\n" + firstName + " " + secondName);
    }

    // Sözün ilk hərfinin böyüdülməsi və geri 'return' edilməsi.
    private static String firstLetterUpperCase(String text){
        return text.substring(0, 1).toUpperCase(Locale.ROOT) + text.substring(1);
    }

}
