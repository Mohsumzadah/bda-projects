package day11.home_practices;

public class Employee extends Personal{


    private Integer id;
    private String department;
    private double salary;

    public Employee(String name, String surname, int age, Integer id, String department, double salary) {
        super(name, surname, age);
        this.id = id;
        this.department = department;
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

}
