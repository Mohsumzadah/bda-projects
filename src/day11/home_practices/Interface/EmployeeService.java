package day11.home_practices.Interface;

import day11.home_practices.Employee;

import java.util.List;

public interface EmployeeService {

    void register(Employee employee);
    void update(int id);
    void delete(int id);
    void findByName(String name);
    void getTotalEmployee();

}
