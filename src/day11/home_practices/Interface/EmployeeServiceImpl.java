package day11.home_practices.Interface;

import day11.home_practices.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class EmployeeServiceImpl implements EmployeeService{

    private List<Employee> employees;

    public EmployeeServiceImpl() {
        this.employees = new ArrayList<>();
    }

    @Override
    public void register(Employee employee) {
        employees.add(employee);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void show(){
        for (Employee employee : employees) {
            System.out.println(employee.getName());

        }
    }
    @Override
    public void update(int id) {
        if (id >= 0 && id < employees.size()) {
            Employee employee = employees.get(id);
            Scanner scanner = new Scanner(System.in);

            System.out.print("Hansı field'i update etmək istəyirsiz? (name, salary, department): ");
            String field = scanner.nextLine();

            switch (field.toLowerCase()) {
                case "name":
                    System.out.print("Yeni adı girin: ");
                    String newName = scanner.nextLine();
                    employee.setName(newName);
                    System.out.println("Ad dəyişdirildi!");
                    break;
                case "salary":
                    System.out.print("Yenu salary girin: ");
                    double newSalary = scanner.nextDouble();
                    employee.setSalary(newSalary);
                    System.out.println("Salary dəyişdirildi!");
                    break;
                case "department":
                    System.out.print("Yeni department girin: ");
                    String newDepartment = scanner.nextLine();
                    employee.setDepartment(newDepartment);
                    System.out.println("Department dəyişdirildi!");
                    break;
                default:
                    System.out.println("Error!");
                    update(id);
                    break;
            }
        } else {
            System.out.println("Yanlış ID!");
        }
}

    @Override
    public void delete(int id) {
        if (id >= 0 && id < employees.size()) {
            Employee employee = employees.get(id);
            employees.remove(employee);
        } else {
            System.out.println("Yanlış ID!");
        }
    }

    @Override
    public void findByName(String name) {
        for (Employee employee : employees) {
            if (employee.getName().toLowerCase(Locale.ROOT)
                    .startsWith(name.toLowerCase(Locale.ROOT))){
                System.out.println(employee.getName());
            }
        }
    }

    @Override
    public void getTotalEmployee() {
        System.out.println("Total işçi sayı: " + employees.size());
    }


}
