package day11.home_practices;

import day11.home_practices.Interface.EmployeeService;
import day11.home_practices.Interface.EmployeeServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static List<Employee> employees = new ArrayList<>();

    EmployeeService employeeService = new EmployeeServiceImpl();
    public static void main(String[] args) {
        EmployeeServiceImpl employeeService = new EmployeeServiceImpl();

        Employee employee1 = new Employee("John", "Doe", 30,1, "IT", 2222);
        Employee employee2 = new Employee("Jadsa", "dsadas", 20,2, "IT", 5000);

        employeeService.register(employee1);
        employeeService.register(employee2);

        employees.addAll(employeeService.getEmployees());

        Scanner scanner = new Scanner(System.in);
        System.out.print("Update etmək istədiyiniz ID ni daxil edin: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consume the newline character

        employeeService.update(id);
        employeeService.findByName("j");

        employeeService.delete(1);
        employeeService.getTotalEmployee();
    }

}
