package day2.home_tasks.part1;

public class Task10_15 {

    /*
    10)
    local dəyişən və “global” necə elan olunurlar və
    arasındakı fərqlər nələrdir.
     */

    private int number = 10; // bu bir global dəyişəndir.
    // funksiyalar içərisində yenidən istifadə oluna bilər.

    private void local(){
        int number2 = 3; // bu bir local dəyişəndir
        // və digər funksiyaların içərisində çağırıla bilməz.
    }

}
