package day2.home_tasks.part1;

public class Task2 {

    public static void main(String[] args) {
        // 2) lifecyle of variable ve  Literals nedir?

        /*
          Lifecycle of variable - dəyişənin ilk yaradılmasına, dəyişdirilməsinə,
          istifadəsinə və yox edilməsinə deyilir. Məsələn:
         */
        String y = "Alma";
        y = "Armud";
        System.out.println(y);

        /*
          Literals - sabit dəyər kodun içinə birbaşa yazılır və sonradan həmin
          dəyəri istifadə edə bilmirik. Məsələn:
         */
        System.out.println("Hi");
        System.out.println(5);
        System.out.println(3.14);

    }
}
