package day2.home_tasks.part1;

public class Task3 {

    public static void main(String[] args) {
//        3) comments nedir novleri , compile olub byte code cevrilirmi commentler?

        /*
            Comment sətirləri istifadə yerləri:
            1) bizdən sonra ki, developerlər üçün kodumuzun
            nə işə yaradığını açıqlamaq üçün

            2) yenidən istifadə edilə biləcək kodları silməmək və yenidən sonra
            istifadə etmək üçün və s.
         */


        /*
            Aşağıda comment sətr tipləri var.
        */

            // 1

            /*
                2
             */

            /**
             *  3
             */

        /*
            Kodlar compile zamanı oxunmur və nəzərə alınmır.
        */


    }
}
