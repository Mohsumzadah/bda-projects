package day2.home_tasks.part1;

public class Task6_9 {

    public static void main(String[] args) {
        /*
        6)
        operator PRECEDENCE, ASSOCIATIVITY nedir
        PRECEDENCE - öncəlikdir məsələn.
        int a = 3 + 2 * 4; burada operator öncəliyi vurmadadır.

        ASSOCIATIVITY - eyni öncəliyə sahib olan operatorların hansı
        sıra ilə əlaqələndiriləcəyini bildirir. məsələn:
        int i = 0, j = 1, k = 2;
        i = j = k;  sıralama sağdan sola doğru olacaq.
        sout(i) –> 2
         */

        /*
        7)
        String Concatenation Operator, Comparison Operators,
         Special Operators, hansilardir

         String Concatenation Operator = System.out.println("Welcome" + userName);
         Comparision operators = System.out.println(x == y); -> true or false
         Special Operators = x++; x--;
         */

        /*
        8)
        Arithmetic operators,•Assignment operators•Logical operators•

        Arithmetic operators = int a = 10;
                                int b = 20;
                                System.out.println(a + b); -> 30

        Assignment operators =
                                x += 3;

        Logical operators =     boolean a = true;
                                boolean b = false;
                                System.out.println(a && b); -> false

         */

        /*
        9)
        Expression Statements, Compound Statements,
        The Empty Statement, Labeled Statements bunlar
         ne ucun lazimdir?ferqleri nelerdir?

        Expression -
        2 + 3; bir expressiondır.
        int a = 2 + 3; artıq bir statementdır.

        Compound -
        birdən çox kodu biryerdə işlətməkdir.
        public static Integer calculate(int a, int b){ başlanğıc
            int total = a + b;
            return total;
        } bitiş

        The Empty Statement - istifadə edilənməyən boş kodlardır.
        while(condition);

        Labeled Statements - bi kod hissəsini taglamaq üçün istifadə olunur
        daha çox swith və forlarda istifadə olunur. Məsələn.
         */
        label1: // <-- bu bir tagdır
        for (int i = 0; i < 5; i++){
            loop2:
            for (int y = 0; y < 3; y++){
                if (y == 2){
                    break label1; // həmin tagı break edirik
                }
            }
        }

    }
}
