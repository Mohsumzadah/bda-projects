package day2.home_tasks.part2;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        /*
        Write a Java program to get the character at the given index within the String.
         */

        System.out.println("Write a word and index.");
        System.out.println("For example: Hello 5");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String text = input.split(" ")[0];
        int index = Integer.parseInt(input.split(" ")[1]);
        char letter = text.charAt(index);

        System.out.println(letter);
    }
}
