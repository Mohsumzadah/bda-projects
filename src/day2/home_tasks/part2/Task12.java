package day2.home_tasks.part2;

public class Task12 {

    public static void main(String[] args) {
        // iki int deyerinin sonuncu elementlerinin cemini tapin.

        int first = 326;
        int second = 22;

        String firstConverted = String.valueOf(first);
        String secondConverted = String.valueOf(second);

        first = Integer.parseInt(
                String.valueOf(
                        firstConverted.charAt(firstConverted.length() - 1)
                ));

        second = Integer.parseInt(
                String.valueOf(
                        secondConverted.charAt(secondConverted.length() - 1)
                ));

        System.out.printf(String.valueOf(first + second));
    }
}
