package day2.home_tasks.part2;

import java.util.Scanner;

public class Task17 {

    public static void main(String[] args) {
        // 2 soz daxil edilir uzunlugu boyuk olan String geriye return edilir.
        //“Hello”, Hello World” -> Hello World return olacaq

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String[] arrays = input.split(" ");

        String longWord = "";
        for (String array : arrays) {
            if (array.length() > longWord.length()){
                longWord = array;
            }
        }
        System.out.println(longWord);
    }
}
