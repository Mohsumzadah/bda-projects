package day2.home_tasks.part2;

import java.util.Scanner;

public class Task18 {
    public static void main(String[] args) {
        //Ele bir method yazin ki: HeLLo WorlD-> chevrilsin olsun hEllO wORLd.
        //Yeni boyuk herfler kichikle, kichik herfler boyukle evez olsun

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        StringBuilder stringBuilder = new StringBuilder();

        for (int index = 0; index < input.length(); index ++){
            char letter = input.charAt(index);
            if (letter == ' '){
                stringBuilder.append(letter);
            } else if (Character.isLowerCase(letter)){
                letter = Character.toUpperCase(letter);
                stringBuilder.append(letter);
            } else if (Character.isUpperCase(letter)) {
                letter = Character.toLowerCase(letter);
                stringBuilder.append(letter);
            }
        }
        System.out.println(stringBuilder);

    }
}
