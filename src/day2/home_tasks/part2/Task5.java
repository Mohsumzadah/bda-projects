package day2.home_tasks.part2;

import java.util.Scanner;

public class Task5 {

    public static void main(String[] args) {
        /*
        Bizdən istifadəçi adını və şifrə tələb edən proqram tərtib edin.
        (Proqramın içində dəyərləri static olaraq təyin edin.)
        Daha sonra username səhv yazıldıqda username yanlışdır.
        Şifrəni səhv yazdıqda şifrə yanlışdır desin.
        Və şifrəni 3-cü dəfədən artıq yanlış yığarsa sistemdən çıxsın.
        Əks halda hər biri doğrudursa “Xoş gəldiniz, ‘username’!” – deyə çap etsin.
        Username hissəsində daxil edilən username yazılsın.
         */
        String limitLogin = "3 dəfə yanlış giriş etdiniz.";
        String wrongUsername = "İstifadəçi adı yanlışdır. Yenidən giriş edin.";
        String wrongPassword = "Parol yanlışdır. Yenidən giriş edin.";

        Scanner scanner = new Scanner(System.in);
        String username = "Admin";
        String password = "12345";

        int wrongLogin = 0;

        while (true){
            System.out.println("İstifadəçi adınızı yazın.");
            String inputName = scanner.nextLine();

            System.out.println("Parolunuzu yazın.");
            String inputPassword = scanner.nextLine();

            if (!inputName.equals(username)){
                wrongLogin++;
                if (wrongLogin != 3) {
                    System.out.println(wrongUsername);
                    System.out.println(" ");
                }else{
                    System.out.println(limitLogin);
                    break;
                }
            } else if (!inputPassword.equals(password)) {
                wrongLogin++;
                if (wrongLogin != 3) {
                    System.out.println(wrongPassword);
                    System.out.println(" ");
                }else{
                    System.out.println(limitLogin);
                    break;
                }
            } else{
                System.out.println("Xoş gəldiniz, ‘" + inputName + "’!");
                break;
            }
        }
    }
}
