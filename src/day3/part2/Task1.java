package day3.part2;

import java.util.Scanner;

public class Task1 {
    /*
    1)Calculator appinin switch və methodlar ilə yazılması.
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true){
            System.out.println("\n");

            System.out.println("Birinci rəqəmi yazın.");
            int birinci = Integer.parseInt(scanner.nextLine());

            System.out.println("Etmək istədiyiniz əməli yazın. ( + - / * )");
            String emel = scanner.nextLine();

            System.out.println("İkinci rəqəmi yazın.");
            int ikinci = Integer.parseInt(scanner.nextLine());


            switch (emel){
                case "+":
                    printLine(topla(birinci, ikinci));
                    break;
                case "-":
                    printLine(cix(birinci, ikinci));
                    break;
                case "/":
                    printLine(bol(birinci, ikinci));
                    break;
                case "*":
                    printLine(vur(birinci, ikinci));
                    break;
            }
        }
    }

    public static void printLine(int reqem){
        System.out.println(reqem);
    }
    public static int topla(int birinciReqem, int ikinciReqem){
        return birinciReqem + ikinciReqem;
    }

    public static int cix(int birinciReqem, int ikinciReqem){
        return birinciReqem - ikinciReqem;
    }

    public static int vur(int birinciReqem, int ikinciReqem){
        return birinciReqem * ikinciReqem;
    }

    public static int bol(int birinciReqem, int ikinciReqem){
        return birinciReqem / ikinciReqem;
    }
}