package day3.part2;

import java.util.Scanner;

public class Task10 {
    /*
    Switch Case-den istf ederek Heftenin gunlerini ve aylari chixartmaq.
     */



    public static void main(String[] args) {
        System.out.println("Həftənin gününü daxil edin:");
        Integer gun = new Scanner(System.in).nextInt();

        System.out.println("Neçənci ay olduğunu daxil edin:");
        Integer ay = new Scanner(System.in).nextInt();

        switch (gun){
            case 1:
                System.out.println("Bazar ertəsi");
                break;
            case 2:
                System.out.println("Çərşənbə axşamı");
                break;
            case 3:
                System.out.println("Çərşənbə");
                break;
            case 4:
                System.out.println("Cümə axşamı");
                break;
            case 5:
                System.out.println("Cümə");
                break;
            case 6:
                System.out.println("Şənbə");
                break;
            case 7:
                System.out.println("Bazar");
                break;
            default:
                System.out.println("Düzgün gün seçilməyib!");
                break;
        }
        switch (ay) {
            case 1:
                System.out.println("Yanvar");
                break;
            case 2:
                System.out.println("Fevral");
                break;
            case 3:
                System.out.println("Mart");
                break;
            case 4:
                System.out.println("Aprel");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("Iyun");
                break;
            case 7:
                System.out.println("Iyul");
                break;
            case 8:
                System.out.println("Avqust");
                break;
            case 9:
                System.out.println("Sentyabr");
                break;
            case 10:
                System.out.println("Oktyabr");
                break;
            case 11:
                System.out.println("Noyabr");
                break;
            case 12:
                System.out.println("Dekabr");
                break;
            default:
                System.out.println("Düzgün ay seçilməyib!");
                break;
        }

    }
}
