package day3.part2;

import java.util.Scanner;

public class Task11 {
    /*
        Verilmish ededin tek ve ya cut olmagini tapin
     */
    public static void main(String[] args) {
        System.out.println("Rəqəm daxil edin:");
        int reqem = new Scanner(System.in).nextInt();

        if (reqem % 2 > 0){
            System.out.println("Tək rəqəm.");
        }else {
            System.out.println("Cüt rəqəm.");
        }
    }
}
