package day3.part2;

import java.util.Scanner;

public class Task12 {
    /*
        Verilmish ededin menfi ya musbet olmagini tapin
     */

    public static void main(String[] args) {
        while (true) {
            System.out.println("Rəqəm daxil edin:");
            int reqem = new Scanner(System.in).nextInt();
            if (reqem < 0) {
                System.out.println("Mənfi");
            } else if (reqem > 0) {
                System.out.println("Müsbət");
            } else {
                System.out.println("Rəqəm sıfırdır.");
            }
        }
    }
}
