package day3.part2;

import java.util.Scanner;

public class Task13 {
    /*
        Iki ferqli tipde eded alin ve onlari toplayin;
     */

    public static void main(String[] args) {
        while (true) {
            System.out.println("Birinci rəqəmi daxil edin:");
            int birinci = new Scanner(System.in).nextInt();

            System.out.println("İkinci rəqəmi daxil edin:");
            int ikinci = new Scanner(System.in).nextInt();

            System.out.println("Cəm: " + topla(birinci, ikinci));

        }
    }

    public static int topla(int a, int b){
        return a + b;
    }
}
