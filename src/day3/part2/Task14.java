package day3.part2;

import java.util.Scanner;

public class Task14 {
    /*
    Iki ferqli tip gotrun ve onlari vurun alinan cavab 100u kechse Sout("100u kechdi yazisi chixardin");
     */

    public static void main(String[] args) {
        System.out.println("Birinci rəqəmi daxil edin:");
        double birinci = Double.parseDouble(new Scanner(System.in).nextLine());
        System.out.println("İkinci rəqəmi daxil edin:");
        int ikinci = Integer.parseInt(new Scanner(System.in).nextLine());

        if (vur(birinci, ikinci) > 100){
            System.out.println("100u kechdi");
        }
    }

    public static int vur(double a, int b){
        return (int) (a * b);
    }

}
