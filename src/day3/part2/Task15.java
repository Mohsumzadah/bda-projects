package day3.part2;

import java.util.Scanner;

public class Task15 {
    /*
    Scannerden iki eded alin ve +,-,*,/, bu sim vollardan hansi gelse o emelyati aparsin(kalkulator);
     */

    public static void main(String[] args) {
        while (true) {
            System.out.println("Birinci rəqəmi daxil edin:");
            int birinci = new Scanner(System.in).nextInt();

            System.out.println("İkinci rəqəmi daxil edin:");
            int ikinci = new Scanner(System.in).nextInt();

            System.out.println("Etmək istədiyiniz əməli daxil edin:");
            String emel = new Scanner(System.in).nextLine();

            switch (emel) {
                case "+":
                    System.out.println("Toplamanın cavabı: " + topla(birinci, ikinci));
                    break;
                case "-":
                    System.out.println("Çıxmanın cavabı: " + cix(birinci, ikinci));
                    break;
                case "/":
                    System.out.println("Bölmənin cavabı: " + bol(birinci, ikinci));
                    break;
                case "*":
                    System.out.println("Vurmanın cavabı: " + vur(birinci, ikinci));
                    break;
                default:
                    System.out.printf("Əməli yanlış yazdınız. (+ - / *)");
            }
        }
    }

    public static int vur(int a, int b){
        return a * b;
    }

    public static int bol(int a, int b){
        return a / b;
    }

    public static int topla(int a, int b){
        return a + b;
    }

    public static int cix(int a, int b){
        return a - b;
    }
}
