package day3.part2;

import java.util.Scanner;

public class Task16 {
    /*
    Scannerden eded girin ve bu eded hem cut eded hem 50 den boyuk olsa "Ugurlu emelyat" bu ikisinden
    birini odemirse "UGURSUZ emelyat" yazsin;
     */

    public static void main(String[] args) {
        System.out.println("Rəqəmi daxil edin:");
        int reqem = new Scanner(System.in).nextInt();

        if (reqem % 2 == 0 && reqem > 50){
            System.out.println("Ugurlu emelyat");
        }else{
            System.out.println("UGURSUZ emelyat");
        }
    }
}
