package day3.part2;

import java.util.Scanner;

public class Task17 {

    /*
    Scannerden eded girin eyer bu eden ya cutdurse yada 100den boyukdurse
    "UGURLU"(her ikisinden biri uygundursa). deyilse "UGURSUZ" yazsin
     */

    public static void main(String[] args) {
        System.out.println("Rəqəmi daxil edin:");
        int reqem = new Scanner(System.in).nextInt();

        if (reqem % 2 == 0 || reqem > 50){
            System.out.println("UGURLU");
        }else{
            System.out.println("UGURSUZ");
        }
    }
}
