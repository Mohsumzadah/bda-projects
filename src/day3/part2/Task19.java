package day3.part2;

import java.util.Scanner;

public class Task19 {
    /*
    Scanner eded daxil edin eyer gelen eded hem 100den boyuk hemde musbetdirse uzerine 15 ededi gelin
     */

    public static void main(String[] args) {
        System.out.println("Rəqəmi daxil edin:");
        int reqem = new Scanner(System.in).nextInt();

        // 100 dən böyük olduğunu check eləmək kifayətdir. (&& ilə yazıla bilər)

        if (reqem > 100){
            reqem+=15;
        }
    }
}
