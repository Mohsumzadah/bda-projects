package day3.part2;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    /*
    2)10luq say sisteminde daxil edilmish ededi 2lik koda cheviren program yazin.
     */

    public static void main(String[] args) {
        System.out.println("Rəqəm daxil edin:");
        int inputNumber = new Scanner(System.in).nextInt();

        System.out.println(convert(inputNumber));
    }


//    private static String convert(int reqem) {
//        String convertedNumber = "";
//        String endNumbers = "";
//        while (reqem / 2 > 0){
//            if (reqem % 2 == 0 && convertedNumber == ""){
//                endNumbers += "0";
//            }else {
//                convertedNumber += reqem % 2;
//            }
//            reqem /= 2;
//        }
//        convertedNumber += reqem % 2;
//
//        if (endNumbers != ""){
//            convertedNumber += endNumbers;
//        }
//
//        String exportString = "";
//        for (int i = convertedNumber.length() -1; i > 0; i--){
//            exportString += convertedNumber.charAt(i);
//        }
//        return exportString;
//    }
    private static String convert(int reqem) {
        String convertedNumber = "";

        while (reqem / 2 > 0){
            convertedNumber += reqem % 2;
            reqem /= 2;
        }
        convertedNumber += reqem % 2;

        String lastReverse = "";

        for (int i = convertedNumber.length() - 1; i >= 0 ; i--) {
            lastReverse += convertedNumber.charAt(i);
        }
        return lastReverse;
    }




}
