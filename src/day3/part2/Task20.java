package day3.part2;

import java.util.Scanner;

public class Task20 {
    //Scannerden eded daxil edin eyer gelen eded ya musbet yada 50den boyukdurse uzerine 10 ededi gelin

    public static void main(String[] args) {
        System.out.println("Rəqəmi daxil edin:");
        int reqem = new Scanner(System.in).nextInt();

        // 0 dan böyük olduğunu check eləmək kifayətdir. (|| ilə yazıla bilər)

        if (reqem > 0){
            reqem+=10;
        }
    }
}
