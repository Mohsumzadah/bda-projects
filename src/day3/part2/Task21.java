package day3.part2;

import java.util.Scanner;

public class Task21 {
    /*
    Boolean tip daxil edin eyer true olsa bu ededleri bir birine vurun(*) yox eyer false olsa bu ededleri toplayin
     */

    public static void main(String[] args) {
        boolean bool = true;

        System.out.println("Birinci rəqəmi daxil edin:");
        int birinci = new Scanner(System.in).nextInt();

        System.out.println("İkinci rəqəmi daxil edin:");
        int ikinci = new Scanner(System.in).nextInt();

        if (bool){
            System.out.println(vur(birinci, ikinci));
        }else {
            System.out.println(topla(birinci, ikinci));

        }
    }

    public static int vur(int a, int b){
        return a * b;
    }

    public static int topla(int a, int b){
        return a + b;
    }
}
