package day3.part2;

public class Task23 {
    /*
    23)methoda daxil edilen reqemin musbet, menfi yoxsa 0 oldugunu teyin eden method yazin.
     */

    private String check(int number){
        if (number == 0){
            return "Sıfır";
        }else if (number < 0){
            return "Mənfi";
        }else {
            return "Müsbət";
        }
    }
}
