package day3.part2;

public class Task24 {
    /*
    Eger menfidirse method geriye -1 qaytarsin, 0-dirsa 0, musbetdirse +1 qaytarsin.
     */

    private String check(int number){
        if (number == 0){
            return "0";
        }else if (number < 0){
            return "-1";
        }else {
            return "+1";
        }
    }
}
