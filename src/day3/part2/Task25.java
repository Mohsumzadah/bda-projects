package day3.part2;

import java.util.Scanner;

public class Task25 {
    /*
    Scanner ile mushteriden 3 reqem isteyin ve en boyuk reqem hansidirsa onu chap edin
     */

    public static void main(String[] args) {
        System.out.println("Birinci rəqəmi daxil edin:");
        int birinci = new Scanner(System.in).nextInt();

        System.out.println("İkinci rəqəmi daxil edin:");
        int ikinci = new Scanner(System.in).nextInt();

        System.out.println("Üçüncü rəqəmi daxil edin:");
        int ucuncu = new Scanner(System.in).nextInt();

        if (birinci > ikinci && birinci > ucuncu){
            System.out.println("Birinci rəqəm böyükdür:" + birinci);
        } else if (ikinci > birinci && ikinci > ucuncu) {
            System.out.println("İkinci rəqəm böyükdür:" + ikinci);

        } else if (ucuncu > birinci && ucuncu > ikinci) {
            System.out.println("Üçüncü rəqəm böyükdür:" + ucuncu);

        }
    }
}
