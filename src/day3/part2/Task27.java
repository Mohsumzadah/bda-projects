package day3.part2;

import java.util.Arrays;
import java.util.List;

public class Task27 {
    /*
    Methoda 4 reqem daxil edilir eger bu reqemlerden her hansisa 2-si bir birine beraberdirse geriye true qaytarsin
     */

    public static void main(String[] args) {
        System.out.println(check(3, 4, 2, 3));
    }
    public static boolean check(int a, int b, int c, int d){
        List<Integer> reqemler = Arrays.asList(a, b, c, d);
        for (Integer yoxlananReqem : reqemler) {

            int eyniReqem = 0;

            for (Integer reqem : reqemler) {
                if (yoxlananReqem == reqem) {
                    eyniReqem += 1;
                }
            }
            if (eyniReqem >= 2) {
                return true;
            }
        }
        return false;
    }

}
