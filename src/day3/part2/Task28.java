package day3.part2;

import java.util.Arrays;
import java.util.List;

public class Task28 {
    /*
    Methoda 4 reqem daxil edilir.a,b,c,d eger ededler artan ardicilliqla daxil edilibse geriye true
    qaytarsin eks halda false
     */

    public static void main(String[] args) {
        System.out.println(check(-54, 0, 5, 100));
    }
    public static boolean check(int a, int b, int c, int d){
        List<Integer> reqemler = Arrays.asList(a, b, c, d);

        int sonuncuReqem = Integer.MIN_VALUE;

        for (Integer yoxlananReqem : reqemler) {
            if (sonuncuReqem == Integer.MIN_VALUE){
                sonuncuReqem = yoxlananReqem;
            } else if (sonuncuReqem < yoxlananReqem) {
               sonuncuReqem = yoxlananReqem;
            }else {
                return false;
            }
        }
        return true;
    }
}
