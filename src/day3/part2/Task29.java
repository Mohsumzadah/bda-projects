package day3.part2;

public class Task29 {
    /*
    Method bir String ve bir int qebul edir ve hemin String-in hemin index-inde olan simvolunu qaytarsin.
    Meselen: Salam, 4 a herfini return edecek
     */

    public static void main(String[] args) {
        System.out.println(returnIndex("Salam", 4));
    }

    public static String returnIndex(String text, int index){
        // verilən şərtdəki məsələn hissəsinə görə 1 ədəd index'i artırmalıyıq.
        index -= 1;
        if (index < 0){
            return "Error";
        }

        String[] arrayText = text.split("");
        return arrayText[index];
    }
}
