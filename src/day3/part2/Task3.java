package day3.part2;

public class Task3 {
    /*
    2lik say sisteminde daxil edilmish ededi 10luga cevirin
     */

    public static void main(String[] args) {
        System.out.println(convert(1010));
    }

    private static int convert(int reqem) {
        String[] formatNumber = String.valueOf(reqem).split("");

        int convertedNumber = 0;

        int leftIndex = formatNumber.length - 1;
        for (String number : formatNumber) {
            double ikiUstu = Math.pow(2, leftIndex);
            int sonReqem = (int) (Integer.parseInt(number) * ikiUstu);
            convertedNumber += sonReqem;
            leftIndex -= 1;
        }

        return convertedNumber;
    }
}
