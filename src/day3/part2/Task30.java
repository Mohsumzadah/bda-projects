package day3.part2;

public class Task30 {

    public static void main(String[] args) {
        System.out.println(check("test", "test"));
    }

    public static boolean check(String text1, String text2){
        return text2.equals(text1);
    }
}
