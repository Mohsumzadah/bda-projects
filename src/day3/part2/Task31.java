package day3.part2;

public class Task31 {
    /*
    31.Method String s, char c, int count qebul edir. S-i count qeder c ile birleshdirir ve geriye return edir
    foo(String s, char c, int count){}
    foo(“Soz”, ‘c’, 5); -> Sozccccc

     */

    public static void main(String[] args) {
        System.out.println(foo("Soz", 'c', 5));
    }

    public static String foo(String s, char c, int count){
        for (int i = 0; i < count; i++){
            s+=c;
        }
        return s;
    }
}
