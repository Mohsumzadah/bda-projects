package day3.part2;

public class Task32 {
    /*
    Method 3 String qebul edir. String a, String b, String c. A ve b-nin ichinde c varsa onda true eks halda false
     */

    public static void main(String[] args) {
        System.out.println(check("Salam", "Sagol", "Sa"));
    }

    public static boolean check(String a, String b, String c){
        return a.contains(c) && b.contains(c);
    }
}
