package day3.part2;

public class Task33 {
    /*
    33.method String a, int begin, int end daxil edilir. A-nin begin ve end arasini alt alta chap edir.
    (Hello World, 3,7) netice olacaq:
    l
    o
    W

     */

    public static void main(String[] args) {
        System.out.println(method("Hello World", 3, 7));
    }

    public static String method(String a, int begin, int end){
        String[] arrayText = a.split("");
        String newText = "";
        for (int i = begin; i< end; i++){
            newText+=arrayText[i];
            newText+="\n";
        }
        return newText;
    }
}
