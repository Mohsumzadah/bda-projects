package day3.part2;

import java.util.Scanner;

public class Task34 {
    /*
    34)Bizdən istifadəçi adını və şifrə tələb edən proqram tərtib edin.
    (Proqramın içində dəyərləri static olaraq təyin edin.) Daha sonra
    - username səhv yazıldıqda username yanlışdır.
    - Şifrəni səhv yazdıqda şifrə yanlışdır desin.
    - Və şifrəni 3-cü dəfədən artıq yanlış yığarsasistemdən çıxsın.
    - Əks halda hər biri doğrudursa “Xoş gəldiniz, ‘username’!” deyə çap etsin.
    Username hissəsində daxil edilən username yazılsın.
     */

    public static String username = "Admin";
    public static String password = "@123";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tryAmount = 0;
        while (true){
            if (tryAmount != 3) {
                System.out.println("Username'i daxil edin:");
                String inputUsername = scanner.nextLine();

                System.out.println("Password'u daxil edin:");
                String inputPassword = scanner.nextLine();

                if (!username.equals(inputUsername) && !password.equals(inputPassword)) {
                    System.out.println("İstifadəçi adı və şifrə yanlışdır.");
                    tryAmount++;
                } else if (!username.equals(inputUsername)) {
                    System.out.println("İstifadəçi adı yanlışdır.");
                    tryAmount++;

                } else if (!password.equals(inputPassword)) {
                    System.out.println("Şifrə yanlışdır.");
                    tryAmount++;

                } else {
                    System.out.println("Xoş gəldiniz, ‘" + inputUsername + "’!");
                }
            }else{
                System.out.println("3 yanlış login.\nSistem bağlandı.");

                break;
            }
        }
    }
}
