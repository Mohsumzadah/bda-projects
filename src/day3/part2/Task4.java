package day3.part8;

import java.util.Scanner;

public class Task4 {
    /*
        10lug say sisteminde daxil edilmish ededi 8liye cevirin
     */
    public static void main(String[] args) {
        System.out.println("Rəqəm daxil edin:");
        int inputNumber = new Scanner(System.in).nextInt();

        System.out.println(convert(inputNumber));
    }


    private static String convert(int reqem) {
        String convertedNumber = "";

        while (reqem / 8 > 0){
            convertedNumber += reqem % 8;
            reqem /= 8;
        }
        convertedNumber += reqem % 8;

        String lastReverse = "";

        for (int i = convertedNumber.length() - 1; i >= 0 ; i--) {
            lastReverse += convertedNumber.charAt(i);
        }
        return lastReverse;
    }
}
