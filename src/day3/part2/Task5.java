package day3.part2;

import java.util.Scanner;

public class Task5 {
    /*
    10lug say sisteminde daxil edilmish ededi 16liga cevirin
     */

    public static void main(String[] args) {
        System.out.println("Rəqəm daxil edin:");
        int inputNumber = new Scanner(System.in).nextInt();

        System.out.println(convert(inputNumber));
    }


    private static String convert(int reqem) {
        String convertedNumber = "";

        while (reqem / 16 > 0){
            switch (reqem % 16){
                case 10:
                    convertedNumber += "a";
                    break;
                case 11:
                    convertedNumber += "b";
                    break;

                case 12:
                    convertedNumber += "c";
                    break;

                case 13:
                    convertedNumber += "d";
                    break;

                case 14:
                    convertedNumber += "e";
                    break;

                case 15:
                    convertedNumber += "f";
                    break;
                default:
                    convertedNumber += reqem % 16;
                    break;
            }

            reqem /= 16;
        }
        convertedNumber += reqem % 16;

        String lastReverse = "";

        for (int i = convertedNumber.length() - 1; i >= 0 ; i--) {
            lastReverse += convertedNumber.charAt(i);
        }
        return lastReverse;
    }
}
