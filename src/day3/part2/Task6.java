package day3.part2;

public class Task6 {
    /*
    6) Boolean tip daxil edin eyer true olsa bu ededleri bir birine vurun(*) yox eyer false olsa bu ededleri toplayin
     */

    public static void main(String[] args) {
        System.out.println(yoxla(5, 8, true));
        System.out.println(yoxla(5, 8, false));
    }

    public static int yoxla(int a, int b, boolean bool){
        if (bool){
            return a * b;
        }else{
            return a + b;
        }
    }
}
