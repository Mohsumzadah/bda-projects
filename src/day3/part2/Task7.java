package day3.part2;

public class Task7 {
    /*
        Biz deyirik ki switch if-den daha suretlidir. Bu ne ucun beledir? Bunu kod numensi yazaraq izah edin
        (mumkun olsa suret vaxtini qeyd edin google search edib  chox maraqlidir)
     */


    public static void main(String[] args) {
        // if dən istifadə etdikdə bütün ifləri tək-tək yoxlayır. Lakin switchdə yerini bildiyi üçün birbaşa lazımı
        // hissəni çağırır.

        int a = 3;

        if (a == 1){
            System.out.println("bir");
        } else if (a == 2) {
            System.out.println("iki");

        } else if (a == 3) {
            System.out.println("üç");
        }

        switch (a){
            case 1:
                System.out.println("bir");
                break;
            case 2:
                System.out.println("iki");
                break;
            case 3:
                System.out.println("üç");
        }
    }
}
