package day3.part2;

public class Task8 {
    /*
        Boolean tipinde deyer gotrun eyer boolean true olsa bashqa yazi deyilse bashqa bir sout verersiz.
     */
    public static void main(String[] args) {
        boolean bool = true;
        String text1 = "Xoş gəldiniz";
        String text2 = "Error";

        if (bool){
            System.out.printf(text1);
        }else {
            System.out.println(text2);
        }
    }
}
