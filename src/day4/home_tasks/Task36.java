package day4.home_tasks;

public class Task36 {

    //36) 0.Yazilmish reqemi soze chevirin meselen: 125 : bir yuz iyirmi besh
    public static void main(String[] args) {
        print(40);
    }

    public static void print(int reqem){
        String[] birler = {"", "bir", "iki", "üç", "dörd", "beş", "altı", "yeddi", "səkkiz", "doqquz"};
        String[] onlar = {"", "on", "iyirmi", "otuz", "qırx", "əlli", "altmış", "yetmiş", "səksən", "doxsan"};

        int yuzler = reqem / 100;
        int onlarBasamagi = (reqem % 100) / 10;
        int birlerBasamagi = reqem % 10;

        String yazi = "";

        if (yuzler > 0) {
            yazi += birler[yuzler] + " yüz ";
        }

        if (onlarBasamagi > 0) {
            yazi += onlar[onlarBasamagi] + " ";
        }

        if (birlerBasamagi > 0) {
            yazi += birler[birlerBasamagi];
        }

        System.out.println(reqem + " : " + yazi);

    }
}
