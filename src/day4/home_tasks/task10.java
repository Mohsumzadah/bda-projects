package day4.home_tasks;

public class task10 {
    /*
        10) Write a Java program to divide two numbers and print on the screen.
            Test Data :
            50/3
            Expected Output :
            16
    */

    public static void main(String[] args) {
        devide(50, 3);
    }

    public static void devide(int a, int b){
        System.out.println(a/b);
    }

}
