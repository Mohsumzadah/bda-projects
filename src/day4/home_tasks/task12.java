package day4.home_tasks;

import java.util.Scanner;

public class task12 {
    /*
    12). Write a Java program that takes two numbers as input and display the product of two numbers.
        Test Data:
        Input first number: 25
        Input second number: 5
        Expected Output :
        25 x 5 = 125
     */

    public static void main(String[] args) {
        System.out.println("Birinci rəqəmi daxil edin:");
        int a = new Scanner(System.in).nextInt();
        System.out.println("İkinci rəqəmi daxil edin:");
        int b = new Scanner(System.in).nextInt();
        System.out.println(product(a,b));
    }
    public static int product(int a, int b){
        return a*b;
    }
}
