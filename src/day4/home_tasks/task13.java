package day4.home_tasks;

import java.util.Scanner;

public class task13 {
    /*

    13)Write a Java program to print the sum (addition), multiply, subtract, divide and remainder of two numbers.
        Test Data:
        Input first number: 125
        Input second number: 24
        Expected Output :
        125 + 24 = 149
        125 - 24 = 101
        125 x 24 = 3000
        125 / 24 = 5
        125 mod 24 = 5


     */

    public static void main(String[] args) {
        System.out.println("Birinci rəqəmi daxil edin:");
        int a = new Scanner(System.in).nextInt();
        System.out.println("İkinci rəqəmi daxil edin:");
        int b = new Scanner(System.in).nextInt();
        print(a, b);
    }
    public static void  print(int a, int b){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(a + " + " + b + " = " + (a+b) + "\n");
        stringBuilder.append(a + " - " + b + " = " + (a-b) + "\n");
        stringBuilder.append(a + " x " + b + " = " + (a*b) + "\n");
        stringBuilder.append(a + " / " + b + " = " + (a/b) + "\n");
        stringBuilder.append(a + " mod " + b + " = " + (a%b) + "\n");
        System.out.println(stringBuilder);
    }

}
