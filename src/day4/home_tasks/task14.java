package day4.home_tasks;

import java.util.Scanner;

public class task14 {
    /*

    14) Write a Java program that takes a number as input and prints its multiplication table upto 10.
        Test Data:
        Input a number: 8
        Expected Output :
        8 x 1 = 8
        8 x 2 = 16
        8 x 3 = 24
        ...
        8 x 10 = 80



     */

    public static void main(String[] args) {
        System.out.println("Birinci rəqəmi daxil edin:");
        int a = new Scanner(System.in).nextInt();
        print(a);
    }
    public static void print(int a){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i < 11; i++) {
            stringBuilder.append(a + " x " + i + " = " + (i*a)+"\n");
        }
        System.out.println(stringBuilder);
    }

}
