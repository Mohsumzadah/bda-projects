package day4.home_tasks;

import java.util.Scanner;

public class task15 {
    /*

    15)Write a Java program to convert temperature from Fahrenheit to Celsius degree.
        Test Data
        Input a degree in Fahrenheit: 212
        Expected Output:
        212.0 degree Fahrenheit is equal to 100.0 in Celsius




     */

    public static void main(String[] args) {
        //formul (F − 32) × 5/9
        System.out.println("Fahrenheit:");
        int a = new Scanner(System.in).nextInt();
        print(a);
    }
    public static void print(int a){
        double cel = (a - 32) * 5/9;
        System.out.println(a + " degree Fahrenheit is equal to "+ cel +" in Celsius");
    }

}
