package day4.home_tasks;

import java.util.Scanner;

public class task16 {
    /*

    16)Write a Java program that reads a number in inches, converts it to meters.
        Note: One inch is 0.0254 meter.
        Test Data
        Input a value for inch: 1000
        Expected Output :
        1000.0 inch is 25.4 meters


     */

    public static void main(String[] args) {
        System.out.println("inch:");
        int a = new Scanner(System.in).nextInt();
        print(a);
    }
    public static void print(int a){
        double meter = a * 0.0254;
        System.out.println(a + " inch is  "+ meter +" meters");
    }

}
