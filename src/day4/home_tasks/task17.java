package day4.home_tasks;

import java.util.Scanner;

public class task17 {
    /*

    17). Write a Java program that reads an integer between 0 and 1000 and adds all the digits in the integer.
        Test Data
        Input an integer between 0 and 1000: 565
        Expected Output :
        The sum of all digits in 565 is 16



     */

    public static void main(String[] args) {
        System.out.println("0 ilə 1000 arasında input daxil edin:");
        int a = new Scanner(System.in).nextInt();
        if (a > 0 && a < 1000) {
            print(a);
        }else {
            System.out.println("Göstərilən aralıqda deyil.");
        }
    }
    public static void print(int a){
        String converted = String.valueOf(a);
        int total = 0;
        for (String s : converted.split("")) {
            total += Integer.parseInt(s);
        }
        System.out.println(total);
    }

}
