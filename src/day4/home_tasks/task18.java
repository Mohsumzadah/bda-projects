package day4.home_tasks;

import java.util.Scanner;

public class task18 {
    /*

    18. Write a Java program to convert minutes into a number of years and days.
        Test Data
        Input the number of minutes: 3456789
        Expected Output :
        3456789 minutes is approximately 6 years and 210 days


     */

    public static void main(String[] args) {
        System.out.println("Dəqiqə daxil edin:");
        int a = new Scanner(System.in).nextInt();
        print(a);
    }
    public static void print(int a){
        int years = a / (365 * 24 * 60);
        int days = (a/60/24) % 365;

        System.out.println(a + "minutes is approximately "+years+" years and "+days+" days");
    }

}
