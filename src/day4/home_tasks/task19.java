package day4.home_tasks;

import java.util.Scanner;

public class task19 {
    /*

    Write a Java program that reads a number and display the square, cube, and fourth power.
        Expected Output:
        Square: .2f
        Cube: .2f
        Fourth power: 50625.00

     */

    public static void main(String[] args) {
        System.out.println("Dəqiqə daxil edin:");
        int a = new Scanner(System.in).nextInt();
        print(a);
    }
    public static void print(float a){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Square: "+Math.pow(a, 2)+"\n");
        stringBuilder.append("Cube: "+Math.pow(a, 3)+"\n");
        stringBuilder.append("Fourth power: "+Math.pow(a, 4)+"\n");
        System.out.println(stringBuilder);
    }

}
