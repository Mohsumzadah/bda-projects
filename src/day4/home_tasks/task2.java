package day4.home_tasks;

public class task2 {
    /*
    Daxil edilmiş ədədə qədər bütün tək ədədləri çap edən proqram yazın.
     */

    public static void main(String[] args) {
        print(22);
    }

    public static void print(int number){
        for (int i = 0; i <= number; i++) {
            if (i % 2 == 1){
                System.out.println(i);
            }
        }
    }
}
