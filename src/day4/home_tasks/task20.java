package day4.home_tasks;

import java.util.Scanner;

public class task20 {
    /*

    Write a Java program that accepts two integers from the user and then prints the sum, the difference, the product, the average, the distance
 (the difference between integer), the maximum (the larger of the two integers), the minimum (smaller of the two integers).
        Test Data
        Input 1st integer: 25
        Input 2nd integer: 5
        Expected Output :
        Sum of two integers: 30
        Difference of two integers: 20
        Product of two integers: 125
        Average of two integers: 15.00
        Distance of two integers: 20
        Max integer: 25
        Min integer: 5


     */

    public static void main(String[] args) {
        System.out.println("Birinci rəqəmi daxil edin:");
        int a = new Scanner(System.in).nextInt();
        System.out.println("İkinci rəqəmi daxil edin:");
        int b = new Scanner(System.in).nextInt();
        print(a, b);
    }
    public static void print(int a, int b){

        System.out.println("Sum of two integers: " + (a + b) + "\n" +
        "Difference of two integers: " + (a - b) + "\n" +
                "Product of two integers: " + (a * b) + "\n" +
                "Average of two integers: " + ((a+b) / 2) + "\n" +
                "Distance of two integers: " + (a - b) + "\n" +
                "Max integer: " + (Math.max(a, b)) + "\n" +
                "Min integer: " + (Math.min(a, b)) + "\n"
        );;
    }

}
