package day4.home_tasks;

import java.util.Scanner;

public class task21 {
    /*

    21) Write a Java program to break an integer into a sequence of individual digits.
        Test Data
        Input six non-negative digits: 123456
        Expected Output :
        1 2 3 4 5 6


     */

    public static void main(String[] args) {
        System.out.println("Dəqiqə daxil edin:");
        String a = new Scanner(System.in).nextLine();
        print(a);
    }

    public static void print(String a){
        System.out.println(a.replace("", " ").trim());
    }
}
