package day4.home_tasks;

public class task22 {
    /*
    22)While Ve For ile 0-dan verilmish edede qeder butun 2-ye bolune bilen ededleri chap et. Ipucu: % ve / istifade edeceksiniz
     */

    public static void main(String[] args) {
        printFor(22);
        printWhile(22);
    }

    public static void printFor(int number){
        for (int i = 0; i <= number; i++) {
            if (i % 2 == 0){
                System.out.println(i);
            }
        }
    }

    public static void printWhile(int number){
        int b = 0;
        while (b <= number){
            if (b % 2 == 0){
                System.out.println(b);
            }
            b++;
        }

    }
}
