package day4.home_tasks;

public class task23 {
    /*
    23) For dovrunden istifade ederek VurmaCedveli yaratmaq.
     */

    public static void main(String[] args) {
        cedvel();
    }

    public static void cedvel(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10 ; j++) {
                stringBuilder.append(i + " x " + j + " = " + (i * j) + "\n");
            }
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder);
    }
}
