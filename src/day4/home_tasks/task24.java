package day4.home_tasks;

public class task24 {
    /*
    24)Verilmish ededin butun ededleri cemini tap.
    12345 - 1+2+3+4+5
     */

    public static void main(String[] args) {

        hesabla(12345);
    }

    public static void hesabla(int a){
        int total = 0;
        for (String s : String.valueOf(a).split("")){
            total += Integer.parseInt(s);
        }
        System.out.println(total);
    }
}
