package day4.home_tasks;

import java.util.Scanner;

public class task29 {
    /*
            29).Verilen string-in tersi ile duzunun bir birine beraber olub olmadigi.
                Meselen "SOS" - "SOS" -> true
                "Hello"-"olleH" -> false



     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextLine());
    }

    public static void check(String input){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(input);
        System.out.println(input.equals(stringBuilder.reverse().toString()));
    }
}
