package day4.home_tasks;

public class task3 {
    /*
    Daxil edilən ədədin bütün rəqəmlərinin cəmini tapan proqram yazın.
     */

    public static void main(String[] args) {
        print(32);
    }

    public static void print(int number){
        String stringNumber = String.valueOf(number);
        int total = 0;
        for (int i = 0; i < stringNumber.length(); i++) {
            total += Integer.parseInt(String.valueOf(stringNumber.charAt(i)));
        }
        System.out.println(total);
    }
}
