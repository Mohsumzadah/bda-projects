package day4.home_tasks;

import java.util.Scanner;

public class task30 {
    /*
            30.Daxil edilen reqemlerin tersi ile duzunun eyni olub olmadigini teyin eden method yazin
                Meselen 545 ve 545 duzdur amma 574 475 sehvdir




     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextLine());
    }

    public static void check(String input){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(input);
        System.out.println(input.equals(stringBuilder.reverse().toString()));
    }
}
