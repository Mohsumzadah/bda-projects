package day4.home_tasks;

import java.util.Scanner;

public class task31 {
    /*
            31).Method 1 reqem, bir simvol ve 1 boolean qebul edir.
                public static void foo(int a, char c, boolean altAlta)
                a qeder c simvolunu chap etsin. altAlta dəyişəni true-dursa altAlta eks halda yan yana chap etsin

     */

    public static void main(String[] args) {

        foo(5, 'a', true);
        foo(5, 'b', false);
    }

    public static void foo(int a, char c, boolean altAlta){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < a; i++) {
            if (altAlta){
                stringBuilder.append(c+"\n");
            }else {
                stringBuilder.append(c);
            }
        }
        System.out.println(stringBuilder);
    }
}
