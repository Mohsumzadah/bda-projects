package day4.home_tasks;

public class task32 {
    /*
            32.method 3 reqem qebul edir a,b,c,
                public static void foo(int a, int b, int c)
                a-dan b-ye qeder butun ededleri c qeder quvvete yukseldir Math.pow-dan istifadə edərək
                foo(1,5,2) -> 1^2=1; 2^2=4; 3^2=9, 4^2=16, 5^2=25

     */

    public static void main(String[] args) {
        foo(1, 5, 2);
    }

    public static void foo(int a, int b, int c){
        String string = "";
        for (int i = a; i < b; i++) {
            string += i + "^" + c + "=" + Math.pow(i, c) + ", ";
        }
        System.out.println(string);
    }
}
