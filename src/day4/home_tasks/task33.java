package day4.home_tasks;

import java.util.Scanner;

public class task33 {
    /*
            33. 0-dan verilmish edede qeder sade ededleri chap et.
                Eded ancaq ozune ve 1-e bolunurse edede sade eded deyilir.
                meselen: 3 reqemi 3-e yeni ozune ve 1-e bolunur ancaq 4 reqemi 4-e yeni ozune,
                1-e ve elave olaraq 2-ye de bolunur. Ipucu: % ve / istifade edeceksiniz


     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextInt());
    }

    public static void check(int a){

        for (int number = 0; number < a; number++) {
            boolean sadedir = true;
            if (number == 1 || number == 0){
                continue;
            }
            for (int i = 2; i <= number / 2 ; i++) {
                if (number % i == 0){
                    sadedir = false;
                    break;
                }
            }
            if (sadedir) System.out.println(number);
        }
    }
}
