package day4.home_tasks;

import java.util.Scanner;

public class task34 {
    /*
            34) 0-dan verilmish edede qeder butun 2-ye bolune bilen ededleri chap et. Ipucu: % ve / istifade edeceksiniz

            meselen: 5 daxil edirem. 0,2 ve 4 reqemleri 2-ye bolunur

     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextInt());
    }

    public static void check(int a){

        for (int number = 0; number < a; number++) {
            if (number % 2 == 0){
                System.out.println(number);
            }
        }
    }
}
