package day4.home_tasks;

import java.util.Scanner;

public class task35 {
    /*
            35)User enters a number and application must find and print all numbers which can be divided by 2 between zero and that given number.
            For example:
            Our given number is 5
            Answer must be 0,2,4. Because each of these numbers can be divided by 2
            Hint: use % and / operators

     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextInt());
    }

    public static void check(int a){

        for (int number = 0; number < a; number++) {
            if (number % 2 == 0){
                System.out.println(number);
            }
        }
    }
}
