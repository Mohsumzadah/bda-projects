package day4.home_tasks;

import java.util.Scanner;

public class task4 {
    /*
    Daxil edilən ədədin sadə yoxsa mürəkkəb olduğunu tapan proqram yazın. (Scanner, for, şərt)
     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextInt());
    }

    public static void check(int number){
        boolean sadeReqemdir = true;

        if (number == 1 || number == 0){
            sadeReqemdir = false;
        }
        for (int i = 2; i <= number / 2 ; i++) {
            if (number % i == 0){
                sadeReqemdir = false;
                break;
            }
        }
        if (sadeReqemdir){
            System.out.println(number + " sadə ədəddir.");
        }else {
            System.out.println(number + " sadə ədəd deyil.");

        }
    }
}
