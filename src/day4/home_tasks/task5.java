package day4.home_tasks;

import java.util.Scanner;

public class task5 {
    /*
        Daxil edilmiş sətirin tərsi ilə düzünün eyni olub olmadığını yoxlayan proqram yazın.
     */

    public static void main(String[] args) {

        check(new Scanner(System.in).nextLine());
    }

    public static void check(String input){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(input);
        System.out.println(input.equals(stringBuilder.reverse().toString()));
    }
}
