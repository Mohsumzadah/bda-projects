package day4.home_tasks;

import java.util.Scanner;

public class task6 {
    /*
        6)Daxil edilən sətirdə hansı simvoldan neçə dəfə istifadə olunduğunu çap edən proqram yazın.
     */

    public static void main(String[] args) {
        System.out.printf("Yazı daxil edin:");
        check(new Scanner(System.in).nextLine());
    }

    public static void check(String input){
        StringBuilder stringBuilder = new StringBuilder();
        String[] arrayInput = input.split("");
        String checkedLetters = "";
        for (String s : arrayInput) {
            if (checkedLetters.contains(s)) continue;
            int count = 0;
            for (String value : arrayInput) {
                if (value.equals(s)) {
                    count++;
                }
            }
            stringBuilder.append(s+" / " + count + "\n");
            checkedLetters += s;
        }
        System.out.println(stringBuilder);
    }
}
