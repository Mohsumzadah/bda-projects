package day4.home_tasks;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class task7 {
    /*
        Daxil edilmiş sətirdə neçə sait və neçə samit olduğunu tapan proqram tərtib edin.
    */

    public static void main(String[] args) {
        System.out.printf("Yazı daxil edin:");
        check(new Scanner(System.in).nextLine());
    }

    public static void check(String input){
        List<String> saitler = Arrays.asList("a", "ı", "o", "u", "e", "ə", "i", "ö", "ü");

        int sait = 0;
        int samit = 0;
        for (String herf : input.split("")){
            if (herf.equals(" ")) continue;
            if (saitler.contains(herf)){
                sait++;
            }else {
                samit++;
            }
        }
        System.out.println(sait + " sait.\n" + samit + " samit.");
    }
}
