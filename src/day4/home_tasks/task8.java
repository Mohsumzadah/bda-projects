package day4.home_tasks;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class task8 {
    /*
        Rekursiyadan istifadə edərək factorial hesablayan proqram tərtib edin.
    */

    public static void main(String[] args) {
        System.out.printf("Yazı daxil edin:");
        System.out.println(factorial(new Scanner(System.in).nextInt()));
    }

    public static int factorial(Integer reqem){
        if (reqem == 0 || reqem == 1){
            return 1;
        } else {
            return reqem * factorial(reqem - 1);
        }
    }
}
