package day4.home_tasks;

import java.util.Scanner;

public class task9 {
    /*
        9)For dövründən istifadə edərək aşağıdakı nəticələri çap edən 3 proqram yazın.
        
        i)	**********
            **********
            **********
            **********         	ii)	*
                                    **
                                    ***
                                    ****
                                    *****	    iii)	        *
                                                               **
                                                              ***
                                                             ****
                                                            ***** 
    */

    public static void main(String[] args) {
        i();
        ii();
        iii();
    }
    
    public static void i(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int setir = 0; setir < 4; setir++) {
            for (int sutun = 0; sutun < 10; sutun++) {
                stringBuilder.append("*");
            }
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder);
    }
    public static void ii(){
        StringBuilder stringBuilder = new StringBuilder();
        int miqdar = 1;
        for (int setir = 0; setir < 5; setir++) {
            for (int sutun = 0; sutun < miqdar; sutun++) {
                stringBuilder.append("*");
            }
            stringBuilder.append("\n");
            miqdar++;
        }

        System.out.println(stringBuilder);

    }
    public static void iii(){
        StringBuilder stringBuilder = new StringBuilder();
        int bosluq = 4;
        int ulduz = 1;
        for (int setir = 0; setir < 5; setir++) {
            for (int bos = 0; bos < bosluq; bos++) {
                stringBuilder.append(" ");
            }
            for (int ul = 0; ul < ulduz; ul++) {
                stringBuilder.append("*");
            }
            stringBuilder.append("\n");
            bosluq--;
            ulduz++;
        }

        System.out.println(stringBuilder);

    }

}
