package day5.home_tasks.link;

import java.util.Arrays;

public class task1 {
    /*
    Write a Java program to sort a numeric array and a string array.
     */

    public static void main(String[] args) {
        int[] arr = {5,4,3,6};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
