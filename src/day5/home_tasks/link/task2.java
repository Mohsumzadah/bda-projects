package day5.home_tasks.link;

import java.util.Arrays;

public class task2 {
    /*
    Write a Java program to sum values of an array.
     */

    public static void main(String[] args) {
        int[] arr = {5,4,3,6};
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        System.out.println(sum);
    }
}
