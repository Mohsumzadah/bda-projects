package day5.home_tasks.link;

import java.util.Arrays;

public class task3 {
    /*
    3. Write a Java program to print the following grid.
            Expected Output :
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
            - - - - - - - - - -
     */
    public static void main(String[] args) {
        String[][] arrays = new String[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                arrays[i][j] = "-";
            }
            System.out.println(Arrays.toString(arrays[i]));
        }


    }
}
