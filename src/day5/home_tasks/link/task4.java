package day5.home_tasks.link;

public class task4 {
    /*
    Write a Java program to calculate the average value of array elements
     */

    public static void main(String[] args) {
        int[] arr = {5,4,3,6};
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        System.out.println(sum/arr.length);
    }
}
