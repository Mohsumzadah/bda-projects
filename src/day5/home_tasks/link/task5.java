package day5.home_tasks.link;

public class task5 {
//Write a Java program to test if an array contains a specific value.

    public static void main(String[] args) {
        int[] arr = {5,4,3,6};
        System.out.println(check(3, arr));

    }

    public static Boolean check(int value, int[] arr){
        for (int i : arr) {
            if (i == value){
                return true;
            }
        }
        return false;
    }
}
