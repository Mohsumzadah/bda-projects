package day5.home_tasks.link;

public class task6 {
    /*
    Write a Java program to find the index of an array element
     */

    public static void main(String[] args) {
        int[] arr = {5,4,3,6};

        check(3, arr);
        
    }

    public static int check(int value, int[] arr){
        for (int i = 0; i < arr.length; i++) {
            if (value == arr[i]){
                return i;
            }
        }
        return -1;
    }
}
