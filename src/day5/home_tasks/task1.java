package day5.home_tasks;

import java.util.Arrays;
import java.util.Scanner;

public class task1 {
    // 1.Daxil edilmish sirasiz massivi sira ile chap edin. (sort)

    public static void main(String[] args) {
        int[] numbers = new int[5];
        for (int i = 0; i < 5; i++) {
            System.out.println("Reqem daxil edin.");
            numbers[i] = new Scanner(System.in).nextInt();
        }
        Arrays.sort(numbers);
        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
