package day5.home_tasks;

public class task11 {
    //11.Write a Java program to sum values of an array.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};
        int sum = 0;
        for (int[] ints : array2D) {
            for (int anInt : ints) {
                sum+=anInt;
            }
        }
        System.out.println(sum);
    }
}
