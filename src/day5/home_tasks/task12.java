package day5.home_tasks;

import java.util.Arrays;

public class task12 {
    //12.Write a Java program to print the following grid.
    //
    //Expected Output :
    //
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -
    //-	- - - - - - - - -

    public static void main(String[] args) {
        String[][] arrays = new String[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                arrays[i][j] = "-";
            }
        }
        System.out.println(Arrays.deepToString(arrays));


    }

}
