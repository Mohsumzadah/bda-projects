package day5.home_tasks;

public class task13 {
    //13)Write a Java program to calculate the average value of array elements.

    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};

        int totalNumber = 0;
        int sum = 0;

        for (int[] ints : array2D) {
            for (int anInt : ints) {
                sum+=anInt;
                totalNumber++;
            }
        }
        System.out.println((sum/totalNumber));
    }

}
