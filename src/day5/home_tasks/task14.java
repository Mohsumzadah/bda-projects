package day5.home_tasks;

import java.util.Scanner;

public class task14 {
    //14)Write a Java program to test if an array contains a specific value.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};

        int inputNum = new Scanner(System.in).nextInt();

        for (int[] ints : array2D) {
            for (int anInt : ints) {
                if (anInt == inputNum){
                    System.out.printf("Bu reqem array icerisinde var");
                    return;
                }
            }
        }
        System.out.printf("Bu reqem array icerisinde yoxdur");

    }

}
