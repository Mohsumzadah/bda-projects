package day5.home_tasks;

import java.util.Scanner;

public class task15 {
    //15)Write a Java program to find the index of an array element.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};

        int inputNum = new Scanner(System.in).nextInt();

        for (int i = 0 ; i < array2D.length; i++) {
            for (int j = 0 ; j < array2D[i].length; j++) {
                if (array2D[i][j] == inputNum){
                    System.out.printf("Index: array2D["+i+"]["+j+"]");
                    return;
                }
            }
        }
        System.out.printf("Bu reqem array icerisinde yoxdur");

    }

}
