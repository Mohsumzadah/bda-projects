package day5.home_tasks;

import java.util.Scanner;

public class task16 {
    //16)Write a Java program to find the maximum and minimum value of an array.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};
        int maximum = 0;

        for (int[] ints : array2D) {
            for (int anInt : ints) {
                if (anInt > maximum){
                    maximum=anInt;
                }
            }
        }
        int minimum = maximum;
        for (int[] ints : array2D) {
            for (int anInt : ints) {
                if (anInt < minimum){
                    minimum=anInt;
                }
            }
        }

        System.out.printf("Maximum: "+maximum + " | " + "Minimum: "+minimum);

    }

}
