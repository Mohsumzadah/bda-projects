package day5.home_tasks;

import java.util.Arrays;

public class task17 {
    //17)Write a Java program to reverse an array of integer values.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};

        for (int[] ints : array2D) {
            int left = 0;
            int right = ints.length;
            int[] reversed = new int[right];
            for (int i = left; i < right; i ++){
                reversed[i] = ints[(right - i) -1];
            }
            ints = reversed;
            System.out.println(Arrays.toString(ints));
        }

    }

}
