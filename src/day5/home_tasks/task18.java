package day5.home_tasks;

import java.util.*;

public class task18 {
    //18)Write a Java program to find the duplicate values of an array of integer values.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};

        Set<Integer> normalList = new HashSet<>();
        List<Integer> duplicated = new ArrayList<>();
        for (int[] ints : array2D) {
            for (int anInt : ints) {
                if (!normalList.add(anInt)){
                    duplicated.add(anInt);
                }
            }
        }
        System.out.println(duplicated);

    }

}
