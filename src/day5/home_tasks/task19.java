package day5.home_tasks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class task19 {
    //18)Write a Java program to find the duplicate values of an array of integer values.
    public static void main(String[] args) {
        String[][] array2D = {{"20","180","33"}, {"15","20","180"}, {"3","5","2"}};

        Set<String> normalList = new HashSet<>();
        List<String> duplicated = new ArrayList<>();
        for (String[] row : array2D) {
            for (String anInt : row) {
                if (!normalList.add(anInt)){
                    duplicated.add(anInt);
                }
            }
        }
        System.out.println(duplicated);

    }

}
