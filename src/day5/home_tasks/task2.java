package day5.home_tasks;

public class task2 {
    // 2.Daxil edilmish massivin sag ve sol diagonal elementlerinin cemini tapin
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,20,1}, {3,5,2}};
        int sum = 0;

        int leftIndex = 0;
        int rightIndex = array2D[0].length - 1;

        for (int[] ints : array2D) {
            sum += ints[leftIndex];
            leftIndex++;
            sum += ints[rightIndex];
            rightIndex--;
        }
        System.out.println(sum);
    }
}
