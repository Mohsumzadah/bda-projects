package day5.home_tasks;

import java.util.Random;

public class task21 {
    /*
    20)scanner ile int massivi doldurulur və bu massiv methoda ötürülür, method bu massivdeki ededleri toplayir
    ve geriye qaytarir. Meselen: {1,2,3,4,5} method return edecek 1+2+3+4+5;
     */

    public static void main(String[] args) {
        int[] array = new int[5];
        for (int i = 0; i < 5 ; i++) {
            array[i] = new Random().nextInt(0, 10);
        }

        cem(array);


    }

    public static void cem(int[] array){
        int cem = 0;
        for (int i : array) {
            cem += i;
        }
        System.out.println(cem);
    }
}
