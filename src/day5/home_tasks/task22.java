package day5.home_tasks;

import java.util.Random;

public class task22 {
    /*
    21). scanner ile int massivi doldurulur və bu massiv methoda ötürülür,
    eger massivdəki reqemlerden her hansisa 2-si bir birine beraberdirse geriye true qaytarsin


     */

    public static void main(String[] args){
        int[] array = {4,4,5,6,7};
        System.out.println(doIt(array));
    }
    public static boolean doIt(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i == j) continue;
                if (arr[i] == arr[j]){
                    return true;
                }
            }
        }

        return false;
        
    }

}
