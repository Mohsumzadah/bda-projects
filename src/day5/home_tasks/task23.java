package day5.home_tasks;

public class task23 {
    // 22). scanner ile int massivi doldurulur və bu massiv methoda oturulur eger ededler artan ardicilliqla
    // daxil edilibse geriye true qaytarsin eks halda false

    public static void main(String[] args){
        int[] array = {3,4,5,6,7};
        System.out.println(doIt(array));
    }
    public static boolean doIt(int[] arr){
        int lastNum = 0;
        for (int reqem : arr) {
            if (lastNum == 0){
                lastNum = reqem;
                continue;
            }
            if (lastNum >= reqem){
                return false;
            }
        }

        return true;

    }
}
