package day5.home_tasks;

import java.util.*;

public class task4 {
    /*
    4.Demek, kichik bir axtarish sistemi quracayiq. String tipinde olan massivde bir chox adlar yer alsin.
    Ve daha sonra consoleda bizden input istenilsin. Meselen:
        String[] users = {""Abbas", ""Leman", ""Xedice", ""Ilyas", ""Nurlan", ""Nihat", ""Elchin", ""Murad",
        ""Mirhesen", ""Emin", ""Farid", ""Terane"};
        - deye bir massivimiz var.
        Men consoledan ‘tera’ – daxil ederken proqram anlasin ki, men teraneni axtariram.
        Ve hemin adi tamamlayaraq yaninda indexide olmaqla chap etsin. Yox eger uygun gelen bir chox ad varsa hamisini
         chap etsin. Eks halda ise bele bir user yoxdur desin.

     */
    public static void main(String[] args) {
        String[] users = {"Abbas", "Leman", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad",
        "Mirhesen", "Emin", "Farid", "Terane"};

        String input = new Scanner(System.in).nextLine().toLowerCase(Locale.ROOT);
        List<String> foundedUsers = new ArrayList<>();
        for (String user : users) {
            if (user.toLowerCase(Locale.ROOT).contains(input)){
                foundedUsers.add(user);
            }
        }
        if (foundedUsers.size() == 0){
            System.out.println("bele bir user yoxdur");
        } else if (foundedUsers.size() > 1) {
                foundedUsers.forEach(user -> System.out.println(user));
        }else {
            int index = Arrays.binarySearch(users, foundedUsers.get(0));
            System.out.println("User: " + foundedUsers.get(0) + " | Index: " + index);
        }
    }
}
