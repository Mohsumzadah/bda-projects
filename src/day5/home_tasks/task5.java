package day5.home_tasks;

public class task5 {
    //5.Daxil edilmiş 2 ölçülü massivin bütün 5-ə bölünən ədədlərini çap edən proqram tərtib edin.
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,110,1}};
        for (int[] ints : array2D) {
            for (int anInt : ints) {
               if (anInt%5 == 0){
                   System.out.println(anInt);
               }
            }
        }
        
    }
}
