package day5.home_tasks;

public class task6 {
    //6.Daxil edilmiş 2 ölçülü massivin bütün elementlərinin cəmini hesablayan proqram yazın
    public static void main(String[] args) {
        int[][] array2D = {{20,18,33}, {15,110,1}};
        int sum = 0;
        for (int[] ints : array2D) {
            for (int anInt : ints) {
               if (anInt%5 == 0){
                   sum+=anInt;
               }
            }
        }
        System.out.println(sum);
        
    }
}
