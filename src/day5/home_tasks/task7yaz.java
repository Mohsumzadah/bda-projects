package day5.home_tasks;

public class task7yaz {
    //7.Daxil edilən 2 ölçülü kvadrat matrisin baş diaqonal elementlərini çap edən proqram yazın. (int[][], for, şərt)
    public static void main(String[] args) {
        int[][] array2D = {{20,18}, {15,110}};
        int sum = 0;

        int leftIndex = 0;
        int rightIndex = array2D[0].length - 1;

        for (int[] ints : array2D) {
            sum += ints[leftIndex];
            leftIndex++;
            sum += ints[rightIndex];
            rightIndex--;
        }
        System.out.println(sum);
    }
}
