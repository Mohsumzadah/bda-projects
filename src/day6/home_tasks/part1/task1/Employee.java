package day6.home_tasks.part1.task1;

public class Employee {
    /*
    1.	Employee classı yaradın. Və içərisində (id, full-name, company, department, position və salary) olsun.
    Daha sonra bir method yaradın, məlumatları içərisinə parametr olaraq göndərək və bizə console da məlumatları
     çap etsin.
     */
    public int id;
    public String fullName;
    public String company;
    public String department;
    public String position;
    public int salary;
    public Employee(int id, String fullName, String company, String department, String position, int salary){
        this.id = id;
        this.fullName = fullName;
        this.company = company;
        this.department = department;
        this.position = position;
        this.salary = salary;
        print();
    }

    private void print(){
        System.out.println("ID: "+ id);
        System.out.println("Full-Name: "+ fullName);
        System.out.println("Company: "+ company);
        System.out.println("Department: "+ department);
        System.out.println("Position: "+ position);
        System.out.println("Salary: "+ salary);
    }

}
