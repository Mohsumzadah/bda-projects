package day6.home_tasks.part1;

import java.util.HashMap;
import java.util.Locale;

public class task10 {
    /*
    10.	String massivinin içərisində bir çox adlar yer alsın. Və biz search methodu düzəldib, həmin massivi və
    axtardığımız adı həmin methoda parametr olaraq göndəririk. (Axtardığımız adı console dan qəbul edirik.)
    Məsələn, mən “id” da göndərsəm başa düşsün ki, Fərid axtarıram. Yox əgər Rəşid də varsa, hər ikisin də
    çap etsin. Yanında indexlərini də göstərsin. (hint: contain methodu)
     */

    private String[] users = {"Abbas", "Leman", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad",
            "Mirhesen", "Emin", "Farid", "Terane"};
    public task10(String input){
        yoxla(input);
    }

    private void yoxla(String input){
        HashMap<String, Integer> userIndex = new HashMap<>();
        for (int i = 0; i < users.length; i++) {
            if (users[i].toLowerCase(Locale.ROOT).contains(input)){
                userIndex.put(users[i], i);
            }
        }
        if (userIndex.size() == 0){
            System.out.println("Bele bir user yoxdur.");
        } else if (userIndex.size() == 1) {
            userIndex.forEach((user, index) -> System.out.println(user));
        }else {
            userIndex.forEach((user, index) -> {
                System.out.println("User: " + user + " | Index: " + index);
            });
        }
    }
}
