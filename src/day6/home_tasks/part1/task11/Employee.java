package day6.home_tasks.part1.task11;

public class Employee {
    /*
    11.Employee classı yaradırıq. İçərisində ad, soyad, depatment, vəzifə və maaş kimi fieldlər qeyd
     olunsun. İçərisinə məlumatları console’dan alaraq constructor’a parametr olaraq göndərək. Və
     bizə göndərdiyimiz məlumatları çap etsin. Həm bütün olaraq, həm də ayrı-ayrılıq da bütün məlumatlar
      əl-çatan olsun.
     */

    /*
    12.Həmin employee classında maaşı artırmaq və azaltmaq kimi methodlar olsun. Yazılmış methodun içərisinə
     dəyəri paramter olaraq göndəririk.
     */

    /*
    13)Və həmçinin vəzifəsini dəyişə biləcəyimiz bir method da əlavə edin. Amma, vəzifə boş ola bilməz.
     */

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getDepartment() {
        return department;
    }

    public String getPosition() {
        return position;
    }

    public Integer getSalary() {
        return salary;
    }

    public Employee(String name, String surname, String department, String position, Integer salary) {
        this.name = name;
        this.surname = surname;
        this.department = department;
        this.position = position;
        this.salary = salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String name;
    public String surname;
    public String department;
    public String position;
    public Integer salary;

    public Employee(){

    }
}
