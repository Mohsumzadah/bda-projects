package day6.home_tasks.part1.task11;

import java.util.Scanner;

public class task11_12_13 {
    //İçərisində ad, soyad, depatment, vəzifə və maaş kimi fieldlər qeyd
    //     olunsun

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Adi daxil edin:");
        String name = sc.nextLine();


        System.out.println("Soyad daxil edin:");
        String surname = sc.nextLine();


        System.out.println("Departamenti daxil edin:");
        String department = sc.nextLine();

        System.out.println("Vezifeni daxil edin:");
        String position = sc.nextLine();

        System.out.println("Maasi daxil edin:");
        Integer salary = sc.nextInt();

        Employee employee = new Employee(name, surname, department, position,salary );
        employee.getName();
        employee.setName("test");
        employee.setPosition("test");
    }

}
