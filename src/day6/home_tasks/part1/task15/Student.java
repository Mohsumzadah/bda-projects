package day6.home_tasks.part1.task15;


public class Student {
    /*
    15)Student classı yaradın. Onun da özünə məxsus lazımi fieldləri olsun.
    Və bütün özəlliklərə və həmçinin hər bir özəlliyə ayrı-ayrılıqda çata bilim.
     Və studenti yaradarkən ad, soyad, ixtisas və universitet yazmaq məcburi olsun.
      Doğum günü kimi şeylər isə istəyə bağlı təyin edə bilək.
     */

    public Student(String ad, String soyad, String ixtisas, String universitet) {
        this.ad = ad;
        this.soyad = soyad;
        this.ixtisas = ixtisas;
        this.universitet = universitet;
    }

    public String getAd() {
        return ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public String getIxtisas() {
        return ixtisas;
    }

    public String getUniversitet() {
        return universitet;
    }

    public String getDogumGunu() {
        return dogumGunu;
    }

    public String ad;
    public String soyad;
    public String ixtisas;
    public String universitet;
    public String dogumGunu;

}
