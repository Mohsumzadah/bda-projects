package day6.home_tasks.part1.task3;

public class CircleCalculation {
    // 3.	Dairənin sahəsini və perimetrini qaytaran class yaradın. (Circle)
        private double radius;

        public CircleCalculation(double radius) {
            this.radius = radius;
        }

        public double getArea() {
            return Math.PI * radius * radius;
        }

        public double getPerimeter() {
            return 2 * Math.PI * radius;
        }

}
