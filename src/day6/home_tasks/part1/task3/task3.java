package day6.home_tasks.part1.task3;

public class task3 {
    public static void main(String[] args) {
        CircleCalculation circle = new CircleCalculation(3);
        System.out.println(circle.getPerimeter());
        System.out.println(circle.getArea());
    }
}
