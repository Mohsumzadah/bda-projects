package day6.home_tasks.part1.task4;

public class Kvadrat {
        private int length;
        private int height;

        public Kvadrat(int length, int height) {
            this.length = length;
            this.height = height;
        }

        public double getPerimetr() {
            return 2* (height + height);
        }

        public double getSahe() {
            return height * length;
        }

}
