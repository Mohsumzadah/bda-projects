package day6.home_tasks.part1.task4;

public class task4 {
    /*
    4.	Area adlı bir class yaradaq. Və biz onun içində olan bir methoda length və height
    göndərək. Bizə həm sahəni, həm də perimetri hesablayıb qaytarsın.
     */
    public static void main(String[] args) {
        Kvadrat kvadrat = new Kvadrat(3, 5);
        System.out.println(kvadrat.getPerimetr());
        System.out.println(kvadrat.getSahe());
    }
}
