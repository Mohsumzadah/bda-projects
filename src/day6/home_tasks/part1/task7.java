package day6.home_tasks.part1;

public class task7 {
    /*
    7.	Bir date classı yaradın, içərisində day, month, year field ləri olsun. Və biz
     onların dəyərini görmək üçün həm ayrı-ayrılıqda çağıra bilək. Həm də ümumi bir
     tarix olaraq göstərə bilək. Və günü, ili, ayı istəyə uyğun dəyişdirə bilək. Hər biri sadə methodlardı.
     */

    private int day;
    private int month;
    private int year;

    public task7(int day, int month, int year){

        this.day = day;
        this.month = month;
        this.year = year;
    }

    public StringBuilder getAllDate(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(day+"-"+month+"-"+year);
        return stringBuilder;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
