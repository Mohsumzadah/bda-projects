package day6.home_tasks.part1;

public class task8 {
    /*
    8.	7-ci sualın eynisini saat üçün edin.
     */

    private int second;
    private int minute;
    private int hour;

    public task8(int second, int minute, int hour){

        this.second = second;
        this.minute = minute;
        this.hour = hour;
    }

    public StringBuilder getClock(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(hour +":"+ minute +"-"+ second);
        return stringBuilder;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int month) {
        this.minute = month;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }
}
