package day6.home_tasks.part1;

public class task9 {
    /*
    9.	Bir book classı yaradın. Və kitaba xas ola biləcək xüsusiyyətləri(field) qeyd edin içində. Bu hissəni
    fantaziyanıza buraxıram. Sadəcə olaraq mən, kitab haqda mənə lazım ola bütün detalları methodlarla çağırıb baxa bilim.
     */

    private String kitabAdi;
    private String sehifeMiqdari;
    private String yazarinAdi;

    public task9(String kitabAdi, String sehifeMiqdari, String yazarinAdi){
        this.kitabAdi = kitabAdi;
        this.sehifeMiqdari = sehifeMiqdari;
        this.yazarinAdi = yazarinAdi;
    }

    public String getKitabAdi(){
        return kitabAdi;
    }

    public String getSehifeMiqdari(){
        return sehifeMiqdari;
    }

    public String getYazarinAdi(){
        return yazarinAdi;
    }

}
