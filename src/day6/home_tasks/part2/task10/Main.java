package day6.home_tasks.part2.task10;

public class Main {
    /*
    10. Bir Human class olsun ve insana aid xususiyyetleri metoda yazin meselen eat() metodu olsun hemin
     metodun ichinde sout("Men yemek yeyirem); yazilsin. (Bu metodlar azi 3dene olsun)
     */

    public static void main(String[] args) {
        Human human = new Human();

        human.eat();
        human.jump();
        human.walk();
    }
}
