package day6.home_tasks.part2.task11;

import day6.home_tasks.part2.task10.Human;

public class Main {
    /*
    11. Iki class yaradin birini Human biri Animal her birinde ayri
    ayri metodlar olsun(xususiyyetleri) ve hemin metodlari mainde chagirin.
     */

    public static void main(String[] args) {
        Human human = new Human();

        human.eat();
        human.jump();
        human.walk();

        Animal animal = new Animal();

        animal.hunt();
        animal.eatHunted();
        animal.drinkWater();
    }
}
