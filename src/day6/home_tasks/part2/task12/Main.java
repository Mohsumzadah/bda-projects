package day6.home_tasks.part2.task12;

public class Main {
    //12. Bir class yaradin ve o classin static ve static olmayan
    // metodlari olsun, daha sonra haminisi main metodda chagirin.

    public static void main(String[] args) {
        Class.staticMethod();

        Class test = new Class();
        test.nonStatic();
    }
}
