package day6.home_tasks.part2.task14;

import com.sun.source.tree.BreakTree;

public class Arithmetic {

    public Arithmetic(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    private int num1;
    private int num2;

    public Integer cem(){
        return num1 + num2;
    }

    public Integer cixma(){
        return num1 - num2;
    }

    public Integer vurma(){
        return num1 * num2;
    }

    public Integer bolme(){
        return num1 / num2;
    }
}
