package day6.home_tasks.part2.task14;

public class Main {
    /*
    14. Bir ayri class yaradin bu clasin ichinde vurma bolme toplama
    chixma metodlari olsun sonra
    main classda chagrilaraq istifade olunsun.
     */
    public static void main(String[] args) {
        Arithmetic arithmetic = new Arithmetic(4, 6);
        System.out.println("Cem: " + arithmetic.cem());
        System.out.println("Cixma:: " + arithmetic.cixma());
        System.out.println("Vurma: " + arithmetic.vurma());
        System.out.println("Bolme: " + arithmetic.bolme());
    }
}
