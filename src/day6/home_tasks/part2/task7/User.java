package day6.home_tasks.part2.task7;

public class User {

    public User(String name, String surname, Integer age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    private String name;
    private String surname;
    private Integer age;
}
