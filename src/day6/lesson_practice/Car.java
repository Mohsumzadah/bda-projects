package day6.lesson_practice;

public class Car {

    public static String test = "aaa";
    private String brand;
    private String model;
    private Integer year;
    public Car(String brand, String model, Integer year){
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
    public String getBrand(){
        return brand;
    }
    public String getModel(){
        return model;
    }
    public Integer getYear(){
        return year;
    }


}
