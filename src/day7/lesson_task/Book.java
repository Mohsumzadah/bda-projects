package day7.lesson_task;

public class Book {
    public Book(String author, int page, int weight) {
        this.author = author;
        this.page = page;
        this.weight = weight;
    }

    private String author;
    private int page;
    private int weight;

    public static void main(String[] args){
        Book b = new Book("",7,2);
        System.out.printf("%d %d %s", b.page, b.weight, b.author);
    }
}
