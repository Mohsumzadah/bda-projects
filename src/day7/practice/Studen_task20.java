package day7.practice;

public class Studen_task20 {
    /*
    20.	Student classı yaradın. Onun da özünə məxsus lazımi fieldləri olsun.
     Və bütün özəlliklərə və həmçinin hər bir özəlliyə ayrı-ayrılıqda çata bilim.
      Və studenti yaradarkən ad, soyad, ixtisas və universitet yazmaq məcburi olsun.
       Doğum günü kimi şeylər isə istəyə bağlı təyin edə bilək.
     */

    String ad;
    String soyad;
    String ixtisas;
    String universited;
    String dogumGunu;
    String dogulduguSeher;

    public Studen_task20(String ad, String soyad, String ixtisas, String universited) {
        this.ad = ad;
        this.soyad = soyad;
        this.ixtisas = ixtisas;
        this.universited = universited;
    }

    public Studen_task20(String ad, String soyad, String ixtisas, String universited, String dogumGunu, String dogulduguSeher) {
        this.ad = ad;
        this.soyad = soyad;
        this.ixtisas = ixtisas;
        this.universited = universited;
        this.dogumGunu = dogumGunu;
        this.dogulduguSeher = dogulduguSeher;
    }
}
