package day7.practice.task12_14;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    /*

    12.	2 class yaradın. Teacher və Student classı. Müəyyən fieldlər yer alsın.
     Və hər bir fieldi ayrı-ayrılıqda çap edə və dəyişmə imkanım olsun

     13.	12-ci sualın davamı olaraq. N sayda student yaradın və hər bir
     fieldini consoledan daxil edin. Və həmin tələbələri çap edin.

    14.	Sonra elə edin ki, mən hər bir tələbənin müəllimini görə bilim.
    (Müəllimləri manual olaraq daxil edin komputerdə) Mən student.getTeachers()
     dedikdə tələbənin hər fənn müəllimlərini görə bilim. Yəni çap olunarkən müəllim və qarşısında tədris etdiyi fənn yer alsın.

     */

    public static List<Student> students = new ArrayList<>();

    public static void main(String[] args) {
        register();
        for (Student student : students) {
            student.getTeachers();
        }
    }

    public static void register(){
        System.out.println("Nece telebe daxil edeceksiniz?:");
        int n = new Scanner(System.in).nextInt();
        for (int i = 0; i < n; i++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Telebe adini daxil edin:");
            String ad = scanner.nextLine();
            System.out.println("Telebe soyadini daxil edin:");
            String soyad = scanner.nextLine();

            System.out.println("Telebenin nece muellimi var?");
            int amount = new Scanner(System.in).nextInt();
            List<Teacher> teachers = new ArrayList<>();
            for (int j = 0; i < amount; i++) {
                System.out.println("Muellimin adi:");
                String muelliminAdi = scanner.nextLine();
                System.out.println("Muellimin soyadi");
                String muelliminSoyadi = scanner.nextLine();
                System.out.println("Muellimin fenni");
                String muelliminFenni = scanner.nextLine();
                Teacher teacher = new Teacher(muelliminAdi, muelliminSoyadi, muelliminFenni);
                teachers.add(teacher);
            }

            Student student = new Student(ad, soyad, teachers);
            students.add(student);
        }
    }

}
