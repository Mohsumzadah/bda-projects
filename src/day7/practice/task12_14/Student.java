package day7.practice.task12_14;

import java.util.List;

public class Student {


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    String surname;
    String name;

    public Student(String surname, String name, List<Teacher> teacherList) {
        this.surname = surname;
        this.name = name;
        this.teacherList = teacherList;
    }

    public void getTeachers(){
        for (Teacher teacher : this.teacherList) {
            System.out.println(teacher.getName());
        }
    }

    List<Teacher> teacherList;

}
