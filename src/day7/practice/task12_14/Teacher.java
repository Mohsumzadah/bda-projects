package day7.practice.task12_14;

public class Teacher {


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    String surname;
    String name;

    public Teacher(String surname, String name, String fenn) {
        this.surname = surname;
        this.name = name;
        this.fenn = fenn;
    }

    String fenn;
}
