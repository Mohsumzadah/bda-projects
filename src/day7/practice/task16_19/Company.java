package day7.practice.task16_19;

import java.util.List;

public class Company {

    String ad;
    Long budce;
    List<Employee> employeeList;
    public Company(String ad, Long budce, List<Employee> employeeList) {
        this.ad = ad;
        this.budce = budce;
        this.employeeList = employeeList;
    }

    public Long getEmployeeSalarys(){
        long sum = 0;
        for (Employee employee : this.employeeList) {
            sum+=employee.getSalary();
        }
        return sum;
    }
    public Boolean budceniAsirmi(){
        return getEmployeeSalarys() > this.budce;
    }
}
