package day7.practice.task16_19;

public class Employee{
    /*
    16.	Employee classı yaradırıq. İçərisində ad, soyad, depatment, vəzifə
     və maaş kimi fieldlər qeyd olunsun. İçərisinə məlumatları console’dan
     alaraq constructor’a parametr olaraq göndərək. Və bizə göndərdiyimiz
     məlumatları çap etsin. Həm bütün olaraq, həm də ayrı-ayrılıq da bütün
     məlumatlar əl-çatan olsun.

     17.	Həmin employee classında maaşı artırmaq və azaltmaq kimi methodlar
      olsun. Yazılmış methodun içərisinə dəyəri paramter olaraq göndəririk.

     18.	Və həmçinin vəzifəsini dəyişə biləcəyimiz bir method da əlavə edin.
      Amma, vəzifə boş ola bilməz.
     */


    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public void salaryArtir(int amount){
        this.salary += amount;
    }

    public void salaryAzalt(int amount){
        this.salary -= amount;
    }

    public void printAllData(){
        System.out.println(this.ad);
        System.out.println(this.soyad);
        System.out.println(this.department);
        System.out.println(this.position);
        System.out.println(this.salary);
    }

    public Employee(String ad, String soyad, String department, String position, Integer salary) {
        this.ad = ad;
        this.soyad = soyad;
        this.department = department;
        this.position = position;
        this.salary = salary;
    }

    String ad;
    String soyad;
    String department;
    String position;
    Integer salary;

}
