package day7.practice.task16_19;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    /*

    19.	Daha sonra n ədəd employee yaradağ. (n dəyərini console’dan almalıyıq
    və hər birini console’dan register edək). Və ayrıca bir Company classımız
    da olsun. Onun fieldləri isə ad, büdcə və işçilərin massivindən ibarət olsun.
     Biz company classının içərisində elə iki method yazaq ki, biri bizə bütün
     işçilərin ümumi maaşınl hesablayıb qaytarsın, toplamda nə qədər edir deyə.
      2ci method isə bu xərclər büdcəni aşır mı, aşmır mı bunu yoxlayıb qaytarsın
      bizə.
     */

    public static List<Employee> employees = new ArrayList<>();
    public static void main(String[] args) {
        register();
    }

    public static void register(){
        System.out.println("Nece telebe daxil edeceksiniz?:");
        int n = new Scanner(System.in).nextInt();
        for (int i = 0; i < n; i++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Adı daxil edin:");
            String ad = scanner.nextLine();
            System.out.println("Soyadı daxil edin:");
            String soyad = scanner.nextLine();
            System.out.println("Departamenti daxil edin:");
            String department = scanner.nextLine();
            System.out.println("Vəzifəni daxil edin:");
            String position = scanner.nextLine();
            System.out.println("Maaşı daxil edin:");
            int salary = scanner.nextInt();

            Employee employee = new Employee(ad, soyad, department, position, salary);

            employees.add(employee);
        }

        Company com = new Company("Test",3000L, employees);

        System.out.println(com.budceniAsirmi());
        System.out.println(com.getEmployeeSalarys());
    }
}
