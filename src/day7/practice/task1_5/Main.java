package day7.practice.task1_5;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    /*

    1) User obyektinin fieldlərini console dan dolduraraq çap edin.

    2) N sayda user obyekti yaradın və proqram run olan zaman console bizdən neçə useri
    register etməyimizi soruşsun. Register edək və məlumatları bizə çap etsin.

    3) Register etdikdən sonra 2 seçim gəlsin qarşımıza. Sistemdən çıx və userləri göstər.
    Onlara vurduqda deyilən iş icra edilsin.

    4)N sayda user massivi yaradın. Və dövrdən istifadə edərək, userlərin sadəcə adlarını çap edin.

    5)Proqramı run etdiyiniz zaman qarşınıza 2 seçim çıxsın. Register et,
    sistemdən çıx. N sayda useri register etdikdən sonra qarşısınıza 2 seçim
     çıxsın. User axtar  və sistemdən çıx. User axtarı vurduğunuz zaman, axtarılan
      ada uyğun olarağ sizə həmin user haqqındakı bütün məlumatları göstərsin.

     */
    public static List<User> users = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("Registration et - 1\n" +
                "Sisteminden cix - 2");
        int firstRun = new Scanner(System.in).nextInt();
        switch (firstRun){
            case 1:
                register();
                settings();
                break;
            case 2:
            default:
                break;
        }

    }

    public static void register(){
        System.out.println("Nece user daxil edeceksiniz?:");
        int n = new Scanner(System.in).nextInt();
        for (int i = 0; i < n; i++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Write your username:");
            String username = scanner.nextLine();
            System.out.println("Write your password:");
            String password = scanner.nextLine();
            User user = new User(username, password);
            users.add(user);
        }
    }

    public static void settings(){
        System.out.println("Userleri goster - 1\n" +
                "Username'leri print et - 2\n" +
                "User axtar - 3\n" +
                "Cixis et - 4\n");
        int input = new Scanner(System.in).nextInt();
        switch (input){
            case 1:
                for (User user : users) {
                    user.printAllData();
                }
                settings();
                break;
            case 2:
                for (User user : users) {
                    System.out.println(user.getUserName());
                }
                settings();
                break;
            case 3:
                System.out.println("User daxil edin:\n");
                String userSearchName = new Scanner(System.in).nextLine();
                searchUser(userSearchName);
            case 4:
            default:
                break;
        }
    }

    private static void searchUser(String userSearchName) {
        boolean tapildi = false;
        for (User user : users) {
            if (user.getUserName().toLowerCase(Locale.ROOT).contains(userSearchName.toLowerCase(Locale.ROOT))){
                user.printAllData();
                tapildi = true;
            }
        }
        if (!tapildi){
            System.out.println("User tapilmadi!");
        }
        settings();
    }
}
