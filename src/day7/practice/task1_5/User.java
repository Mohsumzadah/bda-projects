package day7.practice.task1_5;

public class User {
    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void printAllData(){
        System.out.println(this.userName);
        System.out.println(this.password);
    }


    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    String userName;
    String password;

}
