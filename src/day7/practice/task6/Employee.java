package day7.practice.task6;

public class Employee {
    /*
    Employee classı yaradın içərisində bir çox fieldlər olsun. Və sırf həmin fieldləri dəyişmək və əldə etmək
     üçün methodlar yazın. Bir daha vurğulayıram hər bir field üçün ayrı-ayrılıqda.
     */

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public int getYas() {
        return yas;
    }

    public void setYas(int yas) {
        this.yas = yas;
    }

    String ad;
    String soyad;
    int yas;

}
