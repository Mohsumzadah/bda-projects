package day7.practice.task7;

public class Car {
    // instance variables
    String model;
    String brand;
    int year;
    double price;

    // constructor
    public Car(String model, String brand, int year, double price) {
        this.model = model;
        this.brand = brand;
        this.year = year;
        this.price = price;
    }

    // instance method
    public void printDetails() {
        System.out.println("Model: " + model);
        System.out.println("Brand: " + brand);
        System.out.println("Year: " + year);
        System.out.println("Price: " + price);
    }

    // static method
    public static void printTotalPrice(Car[] cars) {
        double totalPrice = 0;
        for (Car car : cars) {
            totalPrice += car.price;
        }
        System.out.println("Total price of all cars: " + totalPrice);
    }
}
