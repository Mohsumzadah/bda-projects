package day7.practice.task7;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("A4", "Audi", 2022, 45000);
        Car car2 = new Car("Model S", "Tesla", 2021, 85000);

        car1.printDetails();
        car2.printDetails();

        Car[] cars = {car1, car2};

        Car.printTotalPrice(cars);
    }
}