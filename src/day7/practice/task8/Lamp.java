package day7.practice.task8;

public class Lamp {
    private boolean isOn;

    public Lamp() {
        this.isOn = false;
    }

    public void turnOn() {
        this.isOn = true;
        System.out.println("Lamp is turned on.");
    }

    public void turnOff() {
        this.isOn = false;
        System.out.println("Lamp is turned off.");
    }

    public boolean isOn() {
        return this.isOn;
    }
}
