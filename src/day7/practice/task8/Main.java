package day7.practice.task8;

public class Main {
    public static void main(String[] args) {
        Lamp lamp = new Lamp();
        System.out.println("Lamp is on: " + lamp.isOn());
        lamp.turnOn();
        System.out.println("Lamp is on: " + lamp.isOn());
        lamp.turnOff();
        System.out.println("Lamp is on: " + lamp.isOn());
    }
}
