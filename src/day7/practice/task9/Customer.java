package day7.practice.task9;

public class Customer {
    /*
    Customer adlı class yaradın. Və birinci customer, 2-ci
    customer-ə müəyyən məbləğdə pul transfer etsin. Bunun üçün
    hər birinə ayrı-ayrılıqda methodlar yazın. Həm pulu qəbul edən,
    həmdə pulu göndərən.
     */

    Integer money;
    String customerName;

    public Customer(String customerName, Integer money) {
        this.customerName = customerName;
        this.money = money;
    }

    public void transferMoney(Customer otherCustomer, Integer amount){
        if (this.money >= amount){
            this.money -= amount;
        }else{
            System.out.println("No enough money");
            return;
        }

        otherCustomer.money += amount;
        System.out.println("Successfully transfered.");
    }



}
