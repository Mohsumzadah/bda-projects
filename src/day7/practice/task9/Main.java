package day7.practice.task9;

public class Main {

    public static void main(String[] args) {
        Customer cust1 = new Customer("Admin", 5000);
        Customer cust2 = new Customer("User", 3000);
        cust1.transferMoney(cust2, 2000);
        cust2.transferMoney(cust1, 200000);
    }
}
