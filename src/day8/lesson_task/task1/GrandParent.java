package day8.lesson_task.task1;

public class GrandParent {
    static {
        System.out.println("static - grandparent");
    }

    {
        System.out.println("instance - grandparent");
    }

    public GrandParent() {
        System.out.println("constructor - grandparent");
    }
}
