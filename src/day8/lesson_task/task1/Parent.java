package day8.lesson_task.task1;

public class Parent extends GrandParent {
    {
        System.out.println("instance - parent");

    }

    public Parent() {
        System.out.println("constructor - parent");
    }
    static {
        System.out.println("static - parent");
    }
}
