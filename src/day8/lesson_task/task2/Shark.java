package day8.lesson_task.task2;

public class Shark extends Fish{

    private int number = 8;
    public Shark(int age) {
        super(age);
        this.size = 7;
    }

    public void displayShardDetails(){
        System.out.println("Shark age: " + getAge());
        System.out.println("Shark size: " + size);
        System.out.println("Shark number: " + number);
    }

    public static void main(String[] args) {
        new Shark(1).displayShardDetails();
    }

}
