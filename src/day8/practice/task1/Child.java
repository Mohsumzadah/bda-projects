package day8.practice.task1;

public class Child extends Parent{
    private String name;

    public Child(String name, String surname){
        super(surname);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSurname(){
        return returnSurname();
    }
}
