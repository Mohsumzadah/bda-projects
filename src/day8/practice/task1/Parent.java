package day8.practice.task1;

public class Parent {
    private String surname;

    public Parent(String surname){
        this.surname = surname;
    }

    public String returnSurname(){
        return surname;
    }
}
