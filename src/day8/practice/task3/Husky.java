package day8.practice.task3;

public class Husky extends Dog{

    public int getAge(){
        return super.getAge() + 3;
    }

    public static void main(String[] args) {
        System.out.println(new Dog().getAge());
        System.out.println(new Husky().getAge());
    }
}
