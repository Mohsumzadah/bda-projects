package day8.practice.task4;

public class Panda extends Bear{

    public static void eat(){
        System.out.println("Panda is eating");
    }

    public static void sleep(){
        System.out.println("Panda is slepping");
    }

    protected static void wakeUp(){
        System.out.println("Panda wake up");
    }

}
