package day8.practice.task8_11;

public class Infos {
    String surname;
    String name;

    public Infos(String name, String surname){
        this.surname = surname;
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


}
