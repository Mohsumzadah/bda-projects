package day8.practice.task8_11;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    /*
    8.	UniversityManagementSystem’da yaratdığımız modellərin ortağ
     özəlliklərini bir superclassa çıxarın. Və əsas modellərimiz ondan
      extend etsin. (Id xaric)

      9.	UniversityManagementSystem’da hər əməliyyat bitdikdə seçdiyimiz
       section da qalaq. Məsələn, mən müəllim seçib onu register etdimsə,
       birdə başa qayıdıb Məndən student management, teacher management deyə
        soruşmasın. Elə bir başa teacher management sectionları qalsın.


     */

    public static List<Teacher> teachers = new ArrayList<>();
    public static List<Student> students = new ArrayList<>();

    public static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        register();
    }

    public static void register(){
        System.out.println("Muellim qeydiyyati - 1\n" +
                "Telebe qeydiyyati - 2\n" +
                "Müəllimləri göstərmək üçün - 3\n" +
                "Tələbələri göstərmək üçün - 4\n" +
                "Çıxış üçün - 5");
        int qeydiyyatInput = scanner.nextInt();

        switch (qeydiyyatInput){
            case 1:
                muellimQeydiyyat();
                break;
            case 2:
                telebeQeydiyyat();
                break;
            case 3:
                muellimeriGoster();
                break;
            case 4:
                telebeleriGoster();
                break;
            case 5:
            default:
                System.out.println("Error!");
                register();
        }
    }

    private static void telebeleriGoster() {
        for (Student student : students) {
            System.out.println(student.name);
        }
        register();
    }

    private static void muellimeriGoster() {
        for (Teacher teacher : teachers) {
            System.out.println(teacher.name);
        }
        register();

    }

    public static void muellimQeydiyyat(){
        System.out.println("Müəllimin adını daxil edin:");
        String ad = scanner.next();
        if (ad.toLowerCase(Locale.ROOT).equals("exit")){
            register();
            return;
        }

        System.out.println("Müəllimin soyadını daxil edin:");
        String soyad = scanner.next();

        System.out.println("Müəllimin fənnini daxil edin:");
        String fenn = scanner.next()    ;

        Teacher teacher = new Teacher(ad, soyad, fenn);
        teachers.add(teacher);
        System.out.println("Qeydiyyat uğurludur. Davam edə və ya " +
                "geriyə dönmək üçün Exit yaza bilərsiniz.");
        muellimQeydiyyat();
    }

    public static void telebeQeydiyyat(){
        System.out.println("Tələbənin adını daxil edin:");
        String ad = scanner.nextLine();
        if (ad.toLowerCase(Locale.ROOT).equals("exit")){
            register();
            return;
        }

        System.out.println("Tələbənin soyadını daxil edin:");
        String soyad = scanner.nextLine();

        Student student = new Student(ad, soyad);
        students.add(student);

        System.out.println("Qeydiyyat uğurludur. Davam edə və ya " +
                "geriyə dönmək üçün Exit yaza bilərsiniz.");
        telebeQeydiyyat();
    }



}
