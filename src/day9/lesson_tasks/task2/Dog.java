package day9.lesson_tasks.task2;

class Dog extends Animal{
    public Dog(){
        this.age = 6;
    }

    @Override
    public void showAge(){
        System.out.println("alma");
        System.out.println(this.age);
    }

    public void run(){
        System.out.println("Dog running");
    }
}
