package day9.lesson_tasks.task2;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.showAge();
        dog.run();

        Cow cow = new Cow();
        cow.showAge();
    }
}
