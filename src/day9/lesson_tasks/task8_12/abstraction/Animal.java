package day9.lesson_tasks.task8_12.abstraction;

abstract class Animal {
    abstract void talk();
}
