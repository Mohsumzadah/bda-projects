package day9.lesson_tasks.task8_12.encapsulation;

public class Animal {
    private String type;
    private Integer age;


    public Animal(String type, Integer age) {
        this.type = type;
        this.age = age;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
