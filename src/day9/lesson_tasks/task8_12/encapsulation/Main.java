package day9.lesson_tasks.task8_12.encapsulation;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal("a", 1);
        animal.setAge(3);
        animal.setType("b");
    }
}
