package day9.lesson_tasks.task8_12.inheritance;

public class Animal {
    String type;
    Integer age;

    Integer dieAge;


    public Animal(String type, Integer age) {
        this.type = type;
        this.age = age;
    }



    public String getType() {
        return type;
    }

    public int getType(int a ) {
        return age;
    }


    public void setType(String type) {
        this.type = type;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
