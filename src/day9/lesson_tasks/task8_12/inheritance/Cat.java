package day9.lesson_tasks.task8_12.inheritance;

import day9.lesson_tasks.task8_12.encapsulation.Animal;

public class Cat extends Animal {
    public Cat(String type, Integer age) {
        super(type, age);
    }

    public void talk(){
        System.out.println("Meow");
    }
}
