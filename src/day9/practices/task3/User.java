package day9.practices.task3;

public class User extends UserServices{
    /*
    3.	Bir user classı yaradın və onun service classını həmin
     UserService abstract classdan extend etsin. Və daha sonra
      o abstract methodları içərisinə impl edərək doldursun.
      Methodlar: register(), show(). Daha sonra içlərində basic səviyədə doldurun.
     */

    private String username;

    public User(String username) {
        this.username = username;
    }

    @Override
    public void register() {
        System.out.println("Qeydiyyat edildi.");

    }

    @Override
    public void show() {
        System.out.println("User: " + username);
    }



}
