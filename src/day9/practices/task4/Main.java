package day9.practices.task4;

public class Main {
    public static void main(String[] args) {
        Market market = new Market();
        double marketTotalPrice = market.calculateTotalPrice(5, 2.5);
        System.out.println("Market ümumi qiymət: " + marketTotalPrice);

        SuperMarket supermarket = new SuperMarket();
        double supermarketTotalPrice = supermarket.calculateTotalPrice(10, 1.8);
        System.out.println("SuperMarket ümumi qiymət: " + supermarketTotalPrice);
    }
}