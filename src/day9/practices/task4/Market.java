package day9.practices.task4;

public class Market extends Store{
    @Override
    public double calculateTotalPrice(int quantity, double pricePerItem) {
        return quantity * pricePerItem;
    }
}
