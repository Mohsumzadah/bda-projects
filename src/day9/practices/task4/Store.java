package day9.practices.task4;

abstract class Store {
    public abstract double calculateTotalPrice(int quantity, double pricePerItem);
}
