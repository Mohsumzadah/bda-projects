package day9.practices.task4;

public class SuperMarket extends Store{
    @Override
    public double calculateTotalPrice(int quantity, double pricePerItem) {
        return quantity * pricePerItem;
    }
}
