package day9.practices.task5;

public class task5 {
    /*
    5.	Anonymus classa aid nümunə göstərin. Necə olur ki, o işləyir.
     */

    public static void main(String[] args) {
        Hola test = new Hola(){
            @Override
            public void salamVer() {
                System.out.println("Salam");
            }
        };
        test.salamVer();
    }
}
